{
    "Application": "QScheme",
    "Manufacturer": "TwisQ",
    "Url": "https://www.twisq.nl/qscheme",
    "Version": "0.3.3",
    "library": [
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "E0",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "E1",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "E2",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "WP",
                    "number": 7,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "SCL",
                    "number": 6,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 1,
                    "name": "SDA",
                    "number": 5,
                    "position": "R3"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 8,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 4,
                    "position": "B1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "E0",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "E1",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "E2",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "WP",
                    "number": 7,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 1,
                    "name": "SCL",
                    "number": 6,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 1,
                    "name": "SDA",
                    "number": 5,
                    "position": "R3"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 8,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 4,
                    "position": "B1"
                }
            ],
            "price": 0,
            "reference": "IC?",
            "shape": "DIL-08",
            "sizex": 6,
            "sizey": 4,
            "symbolname": "24512",
            "value": "24512"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I0",
                    "number": 2,
                    "position": "L1"
                },
                {
                    "device": 1,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I0",
                    "number": 5,
                    "position": "L1"
                },
                {
                    "device": 2,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I0",
                    "number": 8,
                    "position": "L1"
                },
                {
                    "device": 3,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I0",
                    "number": 11,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I1",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "device": 1,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I1",
                    "number": 6,
                    "position": "L3"
                },
                {
                    "device": 2,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I1",
                    "number": 9,
                    "position": "L3"
                },
                {
                    "device": 3,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I1",
                    "number": 12,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 2,
                    "name": "O",
                    "number": 1,
                    "position": "R2"
                },
                {
                    "device": 1,
                    "electype": 2,
                    "linetype": 2,
                    "name": "O",
                    "number": 4,
                    "position": "R2"
                },
                {
                    "device": 2,
                    "electype": 2,
                    "linetype": 2,
                    "name": "O",
                    "number": 10,
                    "position": "R2"
                },
                {
                    "device": 3,
                    "electype": 2,
                    "linetype": 2,
                    "name": "O",
                    "number": 13,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 14,
                    "position": "T0"
                },
                {
                    "device": 1,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 14,
                    "position": "T0"
                },
                {
                    "device": 2,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 14,
                    "position": "T0"
                },
                {
                    "device": 3,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 14,
                    "position": "T0"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 7,
                    "position": "B0"
                },
                {
                    "device": 1,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 7,
                    "position": "B0"
                },
                {
                    "device": 2,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 7,
                    "position": "B0"
                },
                {
                    "device": 3,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 7,
                    "position": "B0"
                }
            ],
            "converted_png": "iVBORw0KGgoAAAANSUhEUgAAAD0AAAApCAYAAACGJSFkAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAyElEQVRoge3a0QqDMBBE0Zvi//+yfWjFtKQxNmLIzM67sodZFSFphZUJkyD9e+3Se4NRycs6Pf+sTec5bVBAw8vRbFFBb2nyqKGhwaSIhgOXKhoqNmU0lH2PEYMMj3rTUDA6oOHTabnelmib9Ybdatl0oF0SaJs4vb3h7XVCxyfLLYG2icuLLH4tRw8wJA7r/W2MphVT9Cmjf9livZVScy13DnJHNmz1dIJS060Wiaab2s0yLbrnoM0U6NLa9pyISjM801cf+XoCtnpX77TAHY8AAAAASUVORK5CYII=",
            "nrof_devices": 4,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I0",
                    "number": 2,
                    "position": "L1"
                },
                {
                    "device": 1,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I0",
                    "number": 5,
                    "position": "L1"
                },
                {
                    "device": 2,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I0",
                    "number": 8,
                    "position": "L1"
                },
                {
                    "device": 3,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I0",
                    "number": 11,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I1",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "device": 1,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I1",
                    "number": 6,
                    "position": "L3"
                },
                {
                    "device": 2,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I1",
                    "number": 9,
                    "position": "L3"
                },
                {
                    "device": 3,
                    "electype": 1,
                    "linetype": 3,
                    "name": "I1",
                    "number": 12,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 2,
                    "name": "O",
                    "number": 1,
                    "position": "R2"
                },
                {
                    "device": 1,
                    "electype": 2,
                    "linetype": 2,
                    "name": "O",
                    "number": 4,
                    "position": "R2"
                },
                {
                    "device": 2,
                    "electype": 2,
                    "linetype": 2,
                    "name": "O",
                    "number": 10,
                    "position": "R2"
                },
                {
                    "device": 3,
                    "electype": 2,
                    "linetype": 2,
                    "name": "O",
                    "number": 13,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 14,
                    "position": "T0"
                },
                {
                    "device": 1,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 14,
                    "position": "T0"
                },
                {
                    "device": 2,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 14,
                    "position": "T0"
                },
                {
                    "device": 3,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 14,
                    "position": "T0"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 7,
                    "position": "B0"
                },
                {
                    "device": 1,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 7,
                    "position": "B0"
                },
                {
                    "device": 2,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 7,
                    "position": "B0"
                },
                {
                    "device": 3,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 7,
                    "position": "B0"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAAD0AAAApCAYAAACGJSFkAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA/ElEQVRoge2ZSQ7EIAwEy6P8/8vMCSnJLFmwwcZpKTcid9FZkC0FCg4kIL1qLb0L/tJ687v48ZJ2VYFSL/NCpgVuyhzeKzgYwnuGrjKBjwAOyj6jQINy6pHAQclvNGi45ln+LfZwaLmiAqXJc8S0ofE9jwpdlRI8JTTc9B8dGm4wzAANnxyvUUbcapakYcuyjDTSU3JwENto1rTbFgXRmuX5kO01S9LPL+tIMySd7kSW7uz9AJvfOFhnfC9R4b6pqUcWbSNU+t+RoFP1vNNNN1LNsdQnlp6BtWHddk7WkNrjJakFRs+t9kla+pEewGcezZ6bfr5Z1lrI0QT0DYaUo5Lxgn1VAAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "U?",
            "shape": "",
            "sizex": 6,
            "sizey": 4,
            "symbolname": "7402",
            "value": "7402"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "1C0",
                    "number": 6,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 3,
                    "name": "1Y",
                    "number": 7,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "1C1",
                    "number": 5,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "1C2",
                    "number": 4,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "1C3",
                    "number": 3,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "2C0",
                    "number": 10,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 3,
                    "name": "2Y",
                    "number": 9,
                    "position": "R6"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "2C1",
                    "number": 11,
                    "position": "L7"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "2C2",
                    "number": 12,
                    "position": "L8"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "2C3",
                    "number": 13,
                    "position": "L9"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "A",
                    "number": 14,
                    "position": "L11"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "B",
                    "number": 2,
                    "position": "L12"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 2,
                    "name": "1G",
                    "number": 1,
                    "position": "L13"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 2,
                    "name": "2G",
                    "number": 15,
                    "position": "L14"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 16,
                    "position": "T0"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 8,
                    "position": "B0"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "3006869",
            "pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "1C0",
                    "number": 6,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 3,
                    "name": "1Y",
                    "number": 7,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "1C1",
                    "number": 5,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "1C2",
                    "number": 4,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "1C3",
                    "number": 3,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "2C0",
                    "number": 10,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 3,
                    "name": "2Y",
                    "number": 9,
                    "position": "R6"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "2C1",
                    "number": 11,
                    "position": "L7"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "2C2",
                    "number": 12,
                    "position": "L8"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "2C3",
                    "number": 13,
                    "position": "L9"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "A",
                    "number": 14,
                    "position": "L11"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "B",
                    "number": 2,
                    "position": "L12"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 2,
                    "name": "1G",
                    "number": 1,
                    "position": "L13"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 2,
                    "name": "2G",
                    "number": 15,
                    "position": "L14"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 16,
                    "position": "T0"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 8,
                    "position": "B0"
                }
            ],
            "price": 1.1,
            "reference": "IC?",
            "shape": "DIL-16",
            "sizex": 6,
            "sizey": 15,
            "symbolname": "74153",
            "value": "74153"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "RESET",
                    "number": 9,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB7/SCK",
                    "number": 8,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB6/MISO",
                    "number": 7,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB5/MOSI",
                    "number": 6,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB4",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB3",
                    "number": 4,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB2",
                    "number": 3,
                    "position": "L7"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB1",
                    "number": 2,
                    "position": "L8"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB0",
                    "number": 1,
                    "position": "L9"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD7",
                    "number": 21,
                    "position": "L11"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD6",
                    "number": 20,
                    "position": "L12"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD5",
                    "number": 19,
                    "position": "L13"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD4",
                    "number": 18,
                    "position": "L14"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD3",
                    "number": 17,
                    "position": "L15"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD2",
                    "number": 16,
                    "position": "L16"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD1/TXD",
                    "number": 15,
                    "position": "L17"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD0/RXD",
                    "number": 14,
                    "position": "L18"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "XTAL1",
                    "number": 13,
                    "position": "L20"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "XTAL2",
                    "number": 12,
                    "position": "L21"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA7",
                    "number": 33,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA6",
                    "number": 34,
                    "position": "R3"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA5",
                    "number": 35,
                    "position": "R4"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA4",
                    "number": 36,
                    "position": "R5"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA3",
                    "number": 37,
                    "position": "R6"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA2",
                    "number": 38,
                    "position": "R7"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA1",
                    "number": 39,
                    "position": "R8"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA0",
                    "number": 40,
                    "position": "R9"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC7",
                    "number": 29,
                    "position": "R11"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC6",
                    "number": 28,
                    "position": "R12"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC5",
                    "number": 27,
                    "position": "R13"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC4",
                    "number": 26,
                    "position": "R14"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC3",
                    "number": 25,
                    "position": "R15"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC2",
                    "number": 24,
                    "position": "R16"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC1",
                    "number": 23,
                    "position": "R17"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC0",
                    "number": 22,
                    "position": "R18"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "AVCC",
                    "number": 30,
                    "position": "R20"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "AREF",
                    "number": 32,
                    "position": "R21"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 10,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 11,
                    "position": "B1"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 31,
                    "position": "B2"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "RESET",
                    "number": 9,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB7/SCK",
                    "number": 8,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB6/MISO",
                    "number": 7,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB5/MOSI",
                    "number": 6,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB4",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB3",
                    "number": 4,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB2",
                    "number": 3,
                    "position": "L7"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB1",
                    "number": 2,
                    "position": "L8"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PB0",
                    "number": 1,
                    "position": "L9"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD7",
                    "number": 21,
                    "position": "L11"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD6",
                    "number": 20,
                    "position": "L12"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD5",
                    "number": 19,
                    "position": "L13"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD4",
                    "number": 18,
                    "position": "L14"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD3",
                    "number": 17,
                    "position": "L15"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD2",
                    "number": 16,
                    "position": "L16"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD1/TXD",
                    "number": 15,
                    "position": "L17"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PD0/RXD",
                    "number": 14,
                    "position": "L18"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "XTAL1",
                    "number": 13,
                    "position": "L20"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "XTAL2",
                    "number": 12,
                    "position": "L21"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA7",
                    "number": 33,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA6",
                    "number": 34,
                    "position": "R3"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA5",
                    "number": 35,
                    "position": "R4"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA4",
                    "number": 36,
                    "position": "R5"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA3",
                    "number": 37,
                    "position": "R6"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA2",
                    "number": 38,
                    "position": "R7"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA1",
                    "number": 39,
                    "position": "R8"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PA0",
                    "number": 40,
                    "position": "R9"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC7",
                    "number": 29,
                    "position": "R11"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC6",
                    "number": 28,
                    "position": "R12"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC5",
                    "number": 27,
                    "position": "R13"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC4",
                    "number": 26,
                    "position": "R14"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC3",
                    "number": 25,
                    "position": "R15"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC2",
                    "number": 24,
                    "position": "R16"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC1",
                    "number": 23,
                    "position": "R17"
                },
                {
                    "device": 0,
                    "electype": 3,
                    "linetype": 3,
                    "name": "PC0",
                    "number": 22,
                    "position": "R18"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "AVCC",
                    "number": 30,
                    "position": "R20"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "AREF",
                    "number": 32,
                    "position": "R21"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 10,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 11,
                    "position": "B1"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 31,
                    "position": "B2"
                }
            ],
            "price": 0,
            "reference": "Q?",
            "shape": "DIL-20",
            "sizex": 10,
            "sizey": 22,
            "symbolname": "atmega644",
            "value": "ATMEGA644"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 1,
                    "position": "B2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "BASE",
                    "number": 2,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 3,
                    "position": "T2"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "2575349",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 1,
                    "position": "B2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "BASE",
                    "number": 2,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 3,
                    "position": "T2"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAgUlEQVQ4ja3UQQpAIQhF0evf/579o0Ci8pU2q+xQpEIyHNzBs7gY/6nBKmhgbegAAVrQCLagM1hGV2AJ3YHP6Al8QjPwGlVAALupFgWURixT9QJXzzfxZdcfpcBPKXWCS11qBY95qUwjHNMtTZFx6JRO841butS8Vu5Sq/VSl9rt/X8eOg1GY/HWAAAAAElFTkSuQmCC",
            "price": 0.59,
            "reference": "T?",
            "shape": "TO-92D",
            "sizex": 2,
            "sizey": 2,
            "symbolname": "bc517",
            "value": "BC517"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "1",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "2",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "1",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "2",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAALCAYAAACQy8Z9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAALUlEQVQokWNgIBL8Z2D4T6xaJmIVkgIYSXUFEQYyUsusUYAAAxtRA55OaWIoADHHCAVEgIqmAAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "C?",
            "shape": "",
            "sizex": 2,
            "sizey": 1,
            "symbolname": "CAP",
            "value": "CAP"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "1141777",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAALCAYAAACQy8Z9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAALUlEQVQokWNgIBL8Z2D4T6xaJmIVkgIYSXUFEQYyUsusUYAAAxtRA55OaWIoADHHCAVEgIqmAAAAAElFTkSuQmCC",
            "price": 0.08,
            "reference": "C?",
            "shape": "C-02",
            "sizex": 2,
            "sizey": 1,
            "symbolname": "capacitor",
            "value": "100n"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "+",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "-",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "+",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "-",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAW0lEQVQ4jWNgIBL8Z2D4T4wYAwMDAxOxhpICWMjRhOxCGJuRgYGRIoPwiTEw0Mj7RANcrsIGaOJSRlJdQYSBkAijlqEwc2ibTqkZBKOA+oB26ZQYMODZdIQbCgDEKRoHJpNHvgAAAABJRU5ErkJggg==",
            "price": 0,
            "reference": "C?",
            "shape": "",
            "sizex": 2,
            "sizey": 2,
            "symbolname": "capacitor pol",
            "value": "capacitor pol"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 5,
                    "position": "T1"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 8,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 2,
                    "position": "B1"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 3,
                    "position": "B1"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 6,
                    "position": "B1"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 7,
                    "position": "B1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 16,
                    "position": "B5"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 13,
                    "position": "B5"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 12,
                    "position": "B5"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 9,
                    "position": "B5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 15,
                    "position": "T5"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 14,
                    "position": "T5"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 11,
                    "position": "T5"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 10,
                    "position": "T5"
                }
            ],
            "nrof_devices": 4,
            "ordercode": "1045432",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 5,
                    "position": "T1"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 8,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 2,
                    "position": "B1"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 3,
                    "position": "B1"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 6,
                    "position": "B1"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 7,
                    "position": "B1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 16,
                    "position": "B5"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 13,
                    "position": "B5"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 12,
                    "position": "B5"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 9,
                    "position": "B5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 15,
                    "position": "T5"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 14,
                    "position": "T5"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 11,
                    "position": "T5"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 10,
                    "position": "T5"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAADMAAAAVCAYAAADrVNYBAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA7klEQVRYhdWXWw7EIAhFj5Puf8vMVxNiUAEfjfyYKq8reK3gFAHx6q4QAYnEFJDfzoROiYAUKNeDeYEAXA1GA4GLwdRAYDOYXaRhAQE1sSKwFaAO3ErEsrN8eu27gLJrer0ee/YWNYc3vGUQnR/paWAeMOnOaTn3zEXjeMB445gEUKB4WsfVux17r543TpPNeoBOARnlkQ4cLXlU17JdcmZajjMOs4yUYbPt98zr+20XPdbrVuLeeyaU/wrmsr691DzyJRx8AtS7OXOwta2u1Gc/mrOVrgHBR2BWnE/LxzPrNCMzdxS0N+O690yvqn+AZ72ZcCD+/wAAAABJRU5ErkJggg==",
            "price": 1.72,
            "reference": "IC?",
            "shape": "DIL-16",
            "sizex": 5,
            "sizey": 2,
            "symbolname": "cny74-4",
            "value": "CNY74-4"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "R1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "R1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAVElEQVQ4jWNgoAFgxCXxn4HhPxGaceony1B88kwk2UQkgBtKjHfxAWT9tHXpqKFUAyyUxjoygJnFAssV1DAcZtbQCdMRbihOMOiKPhZ8ktTMGBQDACjFFhi8A9AtAAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "X?",
            "shape": "HC18U-1",
            "sizex": 2,
            "sizey": 2,
            "symbolname": "crystal",
            "value": "1MHz"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "A",
                    "number": 4,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "",
                    "number": 1,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "",
                    "number": 2,
                    "position": "T3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "K",
                    "number": 5,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "",
                    "number": 3,
                    "position": "R4"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "A",
                    "number": 4,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "",
                    "number": 1,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "",
                    "number": 2,
                    "position": "T3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "K",
                    "number": 5,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "",
                    "number": 3,
                    "position": "R4"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAAD0AAAA9CAYAAAAeYmHpAAAACXBIWXMAAAsTAAALEwEAmpwYAAABfklEQVRoge2bUQ+DIAyED7P//5fZw9aEOMQClVGO72WZTLmzkKy1BgwgAlH72wCEJ7V857DnbPJsJB0vjeXGpyMC8S6q53HN72tWioZX7wVKUbNArvn0PGpaIlAbaat5U5rumEzaesctotaroWmyWXhcz2yGhcd0zWpYMNc3u2HBTKcXw0K3Xm+GhWbdXg0L1fq9GxaufByjhUzJKlEWbv2sZlg4+6Jc3j+p5TQpXCeqVZtL1r0u95yP9Bjl8j6Az53wvJQ1BCBItF9XA/J9tDgLVD687t1axCflnqaJshCBSBnpbZqFbZqFbZqFbZoFStMAeP6KUiccFDWyYj69co0s/TyA30rDiqQlMco97btxrYDay6pLfD/WybFatNV+VjG+H8pr8B5tumYburYqugY6ulZJuqZYuvbnYXpy/Smj+ZuG1klFcM/5LecJ3aljbXkpJ7j2vN6U1zRfFmFXokoRujvHMrfvfi8rJfcOVXpcw4hCxpDKSM0eHFGteQOR39WwsQ370gAAAABJRU5ErkJggg==",
            "price": 0,
            "reference": "K??",
            "shape": "",
            "sizex": 6,
            "sizey": 6,
            "symbolname": "din5",
            "value": "din5"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 1,
                    "position": "L1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 1,
                    "position": "L1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAAB8AAAAVCAYAAAC+NTVfAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAaUlEQVRIidXTwQ0AIQwDQef67xk+FJDYi9DlHTELEtIfZkmL3JOk70bAFZwOGONkgIVTATZOBER4GhDjSQCCuwFF/90R3l3sRNa5TDXPxZ69C+K4AyO4C8d4Akd4Cts4AVs4BY9xEn4+G/EbGh0qKvImAAAAAElFTkSuQmCC",
            "price": 0.1,
            "reference": "D?",
            "shape": "DIODE-04",
            "sizex": 3,
            "sizey": 2,
            "symbolname": "diode",
            "value": "1N4148"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 1,
                    "position": "T1"
                }
            ],
            "nrof_devices": 0,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "GND",
                    "number": 1,
                    "position": "T1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAALCAYAAACQy8Z9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAALUlEQVQokWP8z8Dwn2HEAkZkDiVBwYhm1tAAOJ1MTFAMTS8jA6KcjxwUA+ZlAKUYCAPUJRX9AAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "",
            "shape": "",
            "sizex": 2,
            "sizey": 1,
            "symbolname": "gnd power",
            "value": "GND"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "1",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "2",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "3",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "4",
                    "number": 4,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "5",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "6",
                    "number": 6,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "7",
                    "number": 7,
                    "position": "L7"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "8",
                    "number": 8,
                    "position": "L8"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "9",
                    "number": 9,
                    "position": "L9"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "10",
                    "number": 10,
                    "position": "L10"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "11",
                    "number": 11,
                    "position": "L11"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "12",
                    "number": 12,
                    "position": "L12"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "13",
                    "number": 13,
                    "position": "L13"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "14",
                    "number": 14,
                    "position": "L14"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "15",
                    "number": 15,
                    "position": "L15"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "16",
                    "number": 16,
                    "position": "L16"
                }
            ],
            "nrof_devices": 0,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "1",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "2",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "3",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "4",
                    "number": 4,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "5",
                    "number": 5,
                    "position": "L5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "6",
                    "number": 6,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "7",
                    "number": 7,
                    "position": "L7"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "8",
                    "number": 8,
                    "position": "L8"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "9",
                    "number": 9,
                    "position": "L9"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "10",
                    "number": 10,
                    "position": "L10"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "11",
                    "number": 11,
                    "position": "L11"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "12",
                    "number": 12,
                    "position": "L12"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "13",
                    "number": 13,
                    "position": "L13"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "14",
                    "number": 14,
                    "position": "L14"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "15",
                    "number": 15,
                    "position": "L15"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "16",
                    "number": 16,
                    "position": "L16"
                }
            ],
            "price": 0,
            "reference": "K?",
            "shape": "",
            "sizex": 3,
            "sizey": 17,
            "symbolname": "header1x16",
            "value": "HEADER 1X16"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "L2"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "L2"
                }
            ],
            "price": 0,
            "reference": "K?",
            "shape": "SCREW-02",
            "sizex": 2,
            "sizey": 3,
            "symbolname": "header2",
            "value": "header2"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 3,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 4,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 5,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 6,
                    "position": "R3"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 3,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 4,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 5,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 6,
                    "position": "R3"
                }
            ],
            "price": 0,
            "reference": "K?",
            "shape": "SIL-2X03",
            "sizex": 2,
            "sizey": 4,
            "symbolname": "header2x3",
            "value": "header2x3"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "1",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "2",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "3",
                    "number": 3,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "4",
                    "number": 4,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "5",
                    "number": 5,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "6",
                    "number": 6,
                    "position": "R3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "7",
                    "number": 7,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "8",
                    "number": 8,
                    "position": "R4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "9",
                    "number": 9,
                    "position": "L5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "10",
                    "number": 10,
                    "position": "R5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "11",
                    "number": 11,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "12",
                    "number": 12,
                    "position": "R6"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "13",
                    "number": 13,
                    "position": "L7"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "14",
                    "number": 14,
                    "position": "R7"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "15",
                    "number": 15,
                    "position": "L8"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "16",
                    "number": 16,
                    "position": "R8"
                }
            ],
            "nrof_devices": 0,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "1",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "2",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "3",
                    "number": 3,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "4",
                    "number": 4,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "5",
                    "number": 5,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "6",
                    "number": 6,
                    "position": "R3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "7",
                    "number": 7,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "8",
                    "number": 8,
                    "position": "R4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "9",
                    "number": 9,
                    "position": "L5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "10",
                    "number": 10,
                    "position": "R5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "11",
                    "number": 11,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "12",
                    "number": 12,
                    "position": "R6"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "13",
                    "number": 13,
                    "position": "L7"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "14",
                    "number": 14,
                    "position": "R7"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "15",
                    "number": 15,
                    "position": "L8"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "16",
                    "number": 16,
                    "position": "R8"
                }
            ],
            "price": 0,
            "reference": "K?",
            "shape": "",
            "sizex": 5,
            "sizey": 9,
            "symbolname": "header2x8",
            "value": "HEADER 2X8"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 4,
                    "position": "L4"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 3,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": " ",
                    "number": 4,
                    "position": "L4"
                }
            ],
            "price": 0,
            "reference": "K?",
            "shape": "SCREW-04",
            "sizex": 2,
            "sizey": 5,
            "symbolname": "header4",
            "value": "header4"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 1,
                    "position": "L1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 2,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 1,
                    "position": "L1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAACkAAAAVCAYAAADb2McgAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAqUlEQVRIidXU2QrAIAxE0Wv//5/tk1DskkziQudRBnNoqIXJqVD7swJF6RzqALXXgyKdT6QF8CYLNZGQh1aoFvSr40K2S1RcP/wNYXXcyAj06es8/TRWR0Iq0OyKr5GR7fLscC8Qgsg2JDpcAUIC2Yb1ZyPexVt/xDs4O+ZaHBcUdX1qUuueCbsmjFwFhCByJRACyNVAEJE7gCAgdwHBidwJBAdyN/A3OQHXGTY0/EhqXQAAAABJRU5ErkJggg==",
            "price": 0,
            "reference": "D?",
            "shape": "",
            "sizex": 4,
            "sizey": 2,
            "symbolname": "LED",
            "value": "LED"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "VCC",
                    "number": 2,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "GND",
                    "number": 1,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 1,
                    "name": "RESET",
                    "number": 3,
                    "position": "R2"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "VCC",
                    "number": 2,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "GND",
                    "number": 1,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 1,
                    "name": "RESET",
                    "number": 3,
                    "position": "R2"
                }
            ],
            "price": 0,
            "reference": "IC?",
            "shape": "TO-92D",
            "sizex": 6,
            "sizey": 4,
            "symbolname": "mc101",
            "value": "MC101"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-PLUS",
                    "number": 3,
                    "position": "L1"
                },
                {
                    "device": 1,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-PLUS",
                    "number": 5,
                    "position": "L1"
                },
                {
                    "device": 2,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-PLUS",
                    "number": 10,
                    "position": "L1"
                },
                {
                    "device": 3,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-PLUS",
                    "number": 12,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-MIN",
                    "number": 2,
                    "position": "L3"
                },
                {
                    "device": 1,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-MIN",
                    "number": 6,
                    "position": "L3"
                },
                {
                    "device": 2,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-MIN",
                    "number": 9,
                    "position": "L3"
                },
                {
                    "device": 3,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-MIN",
                    "number": 13,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 3,
                    "name": "OUTPUT",
                    "number": 1,
                    "position": "R2"
                },
                {
                    "device": 1,
                    "electype": 2,
                    "linetype": 3,
                    "name": "OUTPUT",
                    "number": 7,
                    "position": "R2"
                },
                {
                    "device": 2,
                    "electype": 2,
                    "linetype": 3,
                    "name": "OUTPUT",
                    "number": 8,
                    "position": "R2"
                },
                {
                    "device": 3,
                    "electype": 2,
                    "linetype": 3,
                    "name": "OUTPUT",
                    "number": 14,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-PLUS",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-PLUS",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-PLUS",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-PLUS",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-MIN",
                    "number": 11,
                    "position": "B1"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-MIN",
                    "number": 11,
                    "position": "B1"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-MIN",
                    "number": 11,
                    "position": "B1"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-MIN",
                    "number": 11,
                    "position": "B1"
                }
            ],
            "nrof_devices": 4,
            "ordercode": "1627199",
            "pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-PLUS",
                    "number": 3,
                    "position": "L1"
                },
                {
                    "device": 1,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-PLUS",
                    "number": 5,
                    "position": "L1"
                },
                {
                    "device": 2,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-PLUS",
                    "number": 10,
                    "position": "L1"
                },
                {
                    "device": 3,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-PLUS",
                    "number": 12,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-MIN",
                    "number": 2,
                    "position": "L3"
                },
                {
                    "device": 1,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-MIN",
                    "number": 6,
                    "position": "L3"
                },
                {
                    "device": 2,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-MIN",
                    "number": 9,
                    "position": "L3"
                },
                {
                    "device": 3,
                    "electype": 1,
                    "linetype": 3,
                    "name": "INPUT-MIN",
                    "number": 13,
                    "position": "L3"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 3,
                    "name": "OUTPUT",
                    "number": 1,
                    "position": "R2"
                },
                {
                    "device": 1,
                    "electype": 2,
                    "linetype": 3,
                    "name": "OUTPUT",
                    "number": 7,
                    "position": "R2"
                },
                {
                    "device": 2,
                    "electype": 2,
                    "linetype": 3,
                    "name": "OUTPUT",
                    "number": 8,
                    "position": "R2"
                },
                {
                    "device": 3,
                    "electype": 2,
                    "linetype": 3,
                    "name": "OUTPUT",
                    "number": 14,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-PLUS",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-PLUS",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-PLUS",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-PLUS",
                    "number": 4,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-MIN",
                    "number": 11,
                    "position": "B1"
                },
                {
                    "device": 1,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-MIN",
                    "number": 11,
                    "position": "B1"
                },
                {
                    "device": 2,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-MIN",
                    "number": 11,
                    "position": "B1"
                },
                {
                    "device": 3,
                    "electype": 0,
                    "linetype": 3,
                    "name": "PWR-MIN",
                    "number": 11,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAACkAAAApCAYAAACoYAD2AAAACXBIWXMAAAsTAAALEwEAmpwYAAABB0lEQVRYhc2Yyw7EIAhFr43//8t2RaZj1aIol7OaNpN64gMQFKAUoOADzX9OcQFAApJWlkGWHwlIwG/G5DkCuX4RUfYlKdSyVLQS1D07OzBFdnVAV9nWQDODe8h2D44Wj2iQClBGJ3l20COy1uUefXfXNjAvd4+t28DrhJpm1jvmLcmyssiULDs3q2TZksJQNoqk0JSNJik8ZS+2TI9nnD0WzC3UCSCUZC87hZD8Sp1USW1ep0jOFh2ukqsVkYuktVw7KrmrSj8iufsK8SdpveMcu4xFu8+0MC23V1NrSdK74zYlyWoHqiTZvcphPSkHgt1Ibc4ke+ZqmnEyipyQgbhyQgbiygk3b2yWEqVbLpQAAAAASUVORK5CYII=",
            "price": 2.91,
            "reference": "IC?",
            "shape": "DIL-14",
            "sizex": 4,
            "sizey": 4,
            "symbolname": "mcp6024",
            "value": "MCP6024"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "TR",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "CV",
                    "number": 5,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 3,
                    "name": "Q",
                    "number": 3,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "DIS",
                    "number": 7,
                    "position": "R4"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "THR",
                    "number": 6,
                    "position": "R6"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 2,
                    "name": "R",
                    "number": 4,
                    "position": "T4"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 3,
                    "name": "VCC",
                    "number": 8,
                    "position": "T7"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 3,
                    "name": "GND",
                    "number": 1,
                    "position": "B5"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "TR",
                    "number": 2,
                    "position": "L2"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "CV",
                    "number": 5,
                    "position": "L6"
                },
                {
                    "device": 0,
                    "electype": 2,
                    "linetype": 3,
                    "name": "Q",
                    "number": 3,
                    "position": "R2"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "DIS",
                    "number": 7,
                    "position": "R4"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 3,
                    "name": "THR",
                    "number": 6,
                    "position": "R6"
                },
                {
                    "device": 0,
                    "electype": 1,
                    "linetype": 2,
                    "name": "R",
                    "number": 4,
                    "position": "T4"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 3,
                    "name": "VCC",
                    "number": 8,
                    "position": "T7"
                },
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 3,
                    "name": "GND",
                    "number": 1,
                    "position": "B5"
                }
            ],
            "price": 0,
            "reference": "U?",
            "shape": "",
            "sizex": 8,
            "sizey": 8,
            "symbolname": "NE555",
            "value": "LM555"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 2,
                    "position": "B1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 4,
                    "position": "B5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 5,
                    "position": "T5"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "CATHODE",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "ANODE",
                    "number": 2,
                    "position": "B1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "EMITTER",
                    "number": 4,
                    "position": "B5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "COLLECTOR",
                    "number": 5,
                    "position": "T5"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAADMAAAAVCAYAAADrVNYBAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA7klEQVRYhdWXWw7EIAhFj5Puf8vMVxNiUAEfjfyYKq8reK3gFAHx6q4QAYnEFJDfzoROiYAUKNeDeYEAXA1GA4GLwdRAYDOYXaRhAQE1sSKwFaAO3ErEsrN8eu27gLJrer0ee/YWNYc3vGUQnR/paWAeMOnOaTn3zEXjeMB445gEUKB4WsfVux17r543TpPNeoBOARnlkQ4cLXlU17JdcmZajjMOs4yUYbPt98zr+20XPdbrVuLeeyaU/wrmsr691DzyJRx8AtS7OXOwta2u1Gc/mrOVrgHBR2BWnE/LxzPrNCMzdxS0N+O690yvqn+AZ72ZcCD+/wAAAABJRU5ErkJggg==",
            "price": 0,
            "reference": "ISO?",
            "shape": "",
            "sizex": 5,
            "sizey": 2,
            "symbolname": "OPTOCOUPLER",
            "value": "OPTOCOUPLER"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "A",
                    "number": 1,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "B",
                    "number": 2,
                    "position": "R3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "C",
                    "number": 3,
                    "position": "R4"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "A",
                    "number": 1,
                    "position": "R1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "B",
                    "number": 2,
                    "position": "R3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 2,
                    "name": "C",
                    "number": 3,
                    "position": "R4"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAAD0AAAAzCAYAAAAkaACZAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAwklEQVRoge3YwQqDMBBF0Wfp//9yuipIEZ2GzMyD3LOUaerdRKMEAACAXseQRmhQOn6vDWlcXX8S/c80kRu4m/k3oD04ehNPM9EQi2BpTXRkxiZYWhd9N2cVLK2Nvpq1C5bWR5/nLYOlnOjZ31R5ZS088/yukhbtjOhdEF2le2cvj/6ezFrDs57TkXXawquird7LK6LtTmDZ0ZZn7cxo168q76yFZ76fle3qlbu3C97IdkH0LojeBdG76D3XAgAAYFMfdz1zvS0ifFIAAAAASUVORK5CYII=",
            "price": 0,
            "reference": "J?",
            "shape": "",
            "sizex": 6,
            "sizey": 5,
            "symbolname": "PHONEJACK STEREO",
            "value": "PHONEJACK STEREO"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "1",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "2",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "converted_png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAfCAYAAAAIjIbwAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAK0lEQVRIie3MsREAAAiDQPZfOq4QW4WaeygLpH1XCQu+AIWFBX+CwsLXwQGrET3Ddi5W9AAAAABJRU5ErkJggg==",
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "1",
                    "number": 1,
                    "position": "T1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "2",
                    "number": 2,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAfCAYAAAAIjIbwAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAPklEQVRIie3WsREAIAgEwcOx/5axAYXR1PsU2E8JiiTkaRYQ1e01WpUBjKe2JqKioqKioqKin6CzW+iesV0W/dcIOK22xCoAAAAASUVORK5CYII=",
            "price": 0,
            "reference": "R?",
            "shape": "",
            "sizex": 2,
            "sizey": 3,
            "symbolname": "R",
            "value": "R"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "1",
                    "number": 1,
                    "position": "T5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "2",
                    "number": 2,
                    "position": "T7"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "3",
                    "number": 3,
                    "position": "B6"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "4",
                    "number": 4,
                    "position": "B2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "4",
                    "number": 5,
                    "position": "T2"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "1",
                    "number": 1,
                    "position": "T5"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "2",
                    "number": 2,
                    "position": "T7"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "3",
                    "number": 3,
                    "position": "B6"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "4",
                    "number": 4,
                    "position": "B2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "4",
                    "number": 5,
                    "position": "T2"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAAFsAAAA9CAYAAADCmxXeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAB/UlEQVR4nO2c25KEIAwFw9b+/y+zT2w5lIMQcjuYfp+A7SGiY1lImEpUC1GRrssZtxLV2d/f/Vb6OH4li3nTy12RtXJiwuAx6UpUpcfVqAmb7KuIQlQ0TnJf16M9DrFI9t0YGsmWHgMq2e1gvRJ2TXqIlGsl+6muRbJ3x/vhT8cOr+3kCM51IrzsiKIbq8JDy44surEifFq29f4ZQXRDfOtpKZszlvUFkvObcG0EKdE9TwkPJRtZdGMkfFq21i3xmwiT7BNS3fgWzBCyTxLduBMeQvZbWJKdfXsP92Sf2EIafTjdZXOIcANjguTEuLXQZLf6y8nOvs0Hso2gAilbenVZrVY32SfvQnrayWTJzr7N41a2xgsqCWjPJsLs27CyERm+pDPz7sRbLnISDGU/icy+vgZ0G0Hr29Cy0biVXYjKTC/O/fYamWwkdpIttSqiP3JlP2JN+GzLzr49TyYbjezbc3Uy2Yh4pztisvONKEdStiEhZJ+4fVR//LwrzLvva88lRLLfQijZJ7STUfsIJZsIW/hTnxZv4FIXhtU6GidodXzz/2MjvOV6Qk2Xgd9Wz33wqKvF/dqiNYFoTwc5dWC+pBPlKzYR5vCPxdLyutPcPTaYZF+x/CqZ5DiQsok+D1xjaWvUhJV9pQnpl/nOTZHGagl7BynBSo+1mPMfI2IP+1001QIAAAAASUVORK5CYII=",
            "price": 0,
            "reference": "S?",
            "shape": "",
            "sizex": 9,
            "sizey": 6,
            "symbolname": "rotary encoder sw",
            "value": "rotary encoder sw"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "A",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "WIPER",
                    "number": 2,
                    "position": "T2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "B",
                    "number": 3,
                    "position": "R1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "A",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "WIPER",
                    "number": 2,
                    "position": "T2"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "B",
                    "number": 3,
                    "position": "R1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAADMAAAAVCAYAAADrVNYBAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAf0lEQVRYhe2UQQrAMAgE19L/f9keSiAXiRWNYjMPWB0VAWcYYO9MLVdW4QiOTFVayYSQ+QRCyBCiKo2sIKHXJZ4yHlnajPAHQADt2rRYyLxWAQbYmqkZhpgdNUlrbpkzm4k+uf7fbAfzwCoO7zNDooUM8Ip4ydweIRbabOPwNx6dcS3057OkmwAAAABJRU5ErkJggg==",
            "price": 0,
            "reference": "R?",
            "shape": "",
            "sizex": 5,
            "sizey": 2,
            "symbolname": "rvar",
            "value": "rvar"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "1",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "2",
                    "number": 2,
                    "position": "R1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "1",
                    "number": 1,
                    "position": "L1"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 3,
                    "name": "2",
                    "number": 2,
                    "position": "R1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAAB8AAAAVCAYAAAC+NTVfAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAV0lEQVRIie2UwQoAIAhDZ///z3aLCCwFYwd9Z9k0Z4IACuirRgDx6o2IeTZUc/E85S+ok9dlncW++8i5eDH1z9Blh9DUt4yyGrjp1/1kAPB2zk970zS/mboUI/Vo8opeAAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "SW?",
            "shape": "",
            "sizex": 3,
            "sizey": 2,
            "symbolname": "SW PUSHBUTTON",
            "value": "SW PUSHBUTTON"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 1,
                    "position": "B1"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 4,
                    "linetype": 0,
                    "name": "VCC",
                    "number": 1,
                    "position": "B1"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAABUAAAALCAYAAACQy8Z9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAPElEQVQokWNgoAFgJEbRfwaG/0gaiNJDtIHY+BQbSKzBTBTZSk9DCQJywpQmsY9TAZEuoix5kZKUaBJRAKwhE/xKixZjAAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "",
            "shape": "",
            "sizex": 2,
            "sizey": 1,
            "symbolname": "vcc",
            "value": "VCC"
        },
        {
            "converted_pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 3,
                    "position": "T3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "R4"
                }
            ],
            "nrof_devices": 1,
            "ordercode": "",
            "pins": [
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 1,
                    "position": "L4"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 3,
                    "position": "T3"
                },
                {
                    "device": 0,
                    "electype": 0,
                    "linetype": 1,
                    "name": "",
                    "number": 2,
                    "position": "R4"
                }
            ],
            "png": "iVBORw0KGgoAAAANSUhEUgAAAD0AAAA9CAYAAAAeYmHpAAAACXBIWXMAAAsTAAALEwEAmpwYAAABTUlEQVRoge2b2xKCMAxEt47//8vxxXUQFUJvpt2eV5DmsGFGICR0wADz7puA1LKW5xr12UvuRbbbj7Z92x4OA+ws1f12z/5XOsXDvfQAR6nVgMdsvY6bnASuJl1r3S1ZZ4yL5p7xGqmV1pC1WBSa1xNNmDSrK6owqV5fdGFSrc5RhElxvaMJk+y6RxUml+sfXZj88rj1LiQks6RMTn1mEyZ7L8n2lpR+MWtrk62fbtKzp0zoKZn0kpZB5XomBphk0ktahSWtwpJWYUmrICkNQOevqPSt5cf4RZgxh0JOPbhDjdGICPzyeGvvBKRRBb0YYExc8poee3DtALfLrC2+Xut8Y7a03T6ziK+X8h5GT1tu2EZurEpugE5uVFJuKFZu/LlbPYb6H5MMU8O/xEvXLb517PV4KexjrBYt1+KYf/kCr9VvvXRpkytJ9WjdB0IYu6qZsMnRAAAAAElFTkSuQmCC",
            "price": 0,
            "reference": "K?",
            "shape": "",
            "sizex": 6,
            "sizey": 6,
            "symbolname": "xlr",
            "value": "xlr"
        }
    ],
    "sheet": {
        "component": [
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1555988",
                "posx": 9,
                "posy": 11,
                "price": 0.19,
                "reference": "S1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "PUSH-TACT",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "SW PUSHBUTTON",
                "value": "PUSHBUTTON",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1555988",
                "posx": 9,
                "posy": 15,
                "price": 0.19,
                "reference": "S2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "PUSH-TACT",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "SW PUSHBUTTON",
                "value": "PUSHBUTTON",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1555988",
                "posx": 9,
                "posy": 19,
                "price": 0.19,
                "reference": "S3",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "PUSH-TACT",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "SW PUSHBUTTON",
                "value": "PUSHBUTTON",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1555988",
                "posx": 9,
                "posy": 23,
                "price": 0.19,
                "reference": "S4",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "PUSH-TACT",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "SW PUSHBUTTON",
                "value": "PUSHBUTTON",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1555988",
                "posx": 9,
                "posy": 27,
                "price": 0.19,
                "reference": "S5",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "PUSH-TACT",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "SW PUSHBUTTON",
                "value": "PUSHBUTTON",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 9,
                "posy": 47,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 19,
                "posy": 53,
                "price": 0.04,
                "reference": "R2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329531",
                "posx": 15,
                "posy": 53,
                "price": 0.03,
                "reference": "R1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "470",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 19,
                "posy": 59,
                "price": 1.2,
                "reference": "D1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 29,
                "posy": 59,
                "price": 1.2,
                "reference": "D2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 39,
                "posy": 59,
                "price": 1.2,
                "reference": "D3",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 49,
                "posy": 59,
                "price": 1.2,
                "reference": "D4",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329531",
                "posx": 25,
                "posy": 53,
                "price": 0.03,
                "reference": "R3",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "470",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 29,
                "posy": 53,
                "price": 0.04,
                "reference": "R4",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329531",
                "posx": 35,
                "posy": 53,
                "price": 0.03,
                "reference": "R5",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "470",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 39,
                "posy": 53,
                "price": 0.04,
                "reference": "R6",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329531",
                "posx": 45,
                "posy": 53,
                "price": 0.03,
                "reference": "R7",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "470",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 49,
                "posy": 53,
                "price": 0.04,
                "reference": "R8",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 31,
                "posy": 49,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329500",
                "posx": 64,
                "posy": 100,
                "price": 0.03,
                "reference": "R11",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "220",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 61,
                "posy": 97,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": true,
                "ordercode": "2787097",
                "posx": 72,
                "posy": 104,
                "price": 1.13,
                "reference": "K4",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X05",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "din5",
                "value": "DIN5",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": true,
                "ordercode": "2787097",
                "posx": 72,
                "posy": 126,
                "price": 1.13,
                "reference": "K5",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X05",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "din5",
                "value": "DIN5",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329500",
                "posx": 64,
                "posy": 134,
                "price": 0.03,
                "reference": "R14",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "220",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329500",
                "posx": 64,
                "posy": 122,
                "price": 0.03,
                "reference": "R13",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "220",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329500",
                "posx": 64,
                "posy": 112,
                "price": 0.03,
                "reference": "R12",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "220",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2064978",
                "posx": 7,
                "posy": 36,
                "price": 5.71,
                "reference": "S6",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "ROTENC",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "rotary encoder sw",
                "value": "ROTARY-ENCODER",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2751377",
                "posx": 71,
                "posy": 43,
                "price": 0.2,
                "reference": "K14",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-2X03",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "header2x3",
                "value": "HEADER-2X3",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 78,
                "posy": 49,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 13,
                "posy": 137,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1469547",
                "posx": 21,
                "posy": 104,
                "price": 0.78,
                "reference": "IC3",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-06",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "OPTOCOUPLER",
                "value": "CNY17-4",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329534",
                "posx": 25,
                "posy": 96,
                "price": 0.03,
                "reference": "R10",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "4k7",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 25,
                "posy": 92,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2787097",
                "posx": 5,
                "posy": 102,
                "price": 1.13,
                "reference": "K3",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X05",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "din5",
                "value": "DIN5",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329531",
                "posx": 15,
                "posy": 98,
                "price": 0.03,
                "reference": "R9",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "470",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 25,
                "posy": 112,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1835537",
                "posx": 5,
                "posy": 83,
                "price": 1.38,
                "reference": "K2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "PHONEJACK-STEREO",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "PHONEJACK STEREO",
                "value": "JACK-6M3-F",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1835537",
                "posx": 5,
                "posy": 76,
                "price": 1.38,
                "reference": "K1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "PHONEJACK-STEREO",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "PHONEJACK STEREO",
                "value": "JACK-6M3-F",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 17,
                "posy": 88,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1288340",
                "posx": 70,
                "posy": 54,
                "price": 7.18,
                "reference": "IC1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-40",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "atmega644",
                "value": "ATMEGA644",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 96,
                "posy": 8,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "9339558",
                "posx": 90,
                "posy": 12,
                "price": 0.03,
                "reference": "R17",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "47k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329473",
                "posx": 109,
                "posy": 21,
                "price": 0.07,
                "reference": "R18",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "100",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 106,
                "posy": 31,
                "price": 0.15,
                "reference": "C4",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "9353844",
                "posx": 86,
                "posy": 27,
                "price": 2.91,
                "reference": "R15",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X03",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "rvar",
                "value": "ALT-10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "9353844",
                "posx": 90,
                "posy": 19,
                "price": 2.91,
                "reference": "R16",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X03",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "rvar",
                "value": "ALT-10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 94,
                "posy": 36,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 79,
                "posy": 40,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2308718",
                "posx": 74,
                "posy": 80,
                "price": 0.22,
                "reference": "X1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "HC18U-1",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "crystal",
                "value": "20MHz",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141760\n",
                "posx": 78,
                "posy": 83,
                "price": 0.14,
                "reference": "C2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-01",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor",
                "value": "22p",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141760\n",
                "posx": 70,
                "posy": 83,
                "price": 0.14,
                "reference": "C1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-01",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor",
                "value": "22p",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 74,
                "posy": 87,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 154,
                "posy": 23,
                "price": 1.2,
                "reference": "D5",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 154,
                "posy": 14,
                "price": 0.04,
                "reference": "R20",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 154,
                "posy": 8,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "9473912",
                "posx": 117,
                "posy": 30,
                "price": 0.22,
                "reference": "R19",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329486",
                "posx": 163,
                "posy": 14,
                "price": 0.07,
                "reference": "R21",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "1k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "9353844",
                "posx": 163,
                "posy": 22,
                "price": 2.91,
                "reference": "R22",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "RPOT-MIDIATOR",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "rvar",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 270,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1144701",
                "posx": 188,
                "posy": 24,
                "price": 0.62,
                "reference": "C7",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "CE10-02R",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor pol",
                "value": "470u",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 270,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1144701",
                "posx": 188,
                "posy": 41,
                "price": 0.62,
                "reference": "C8",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "CE10-02R",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor pol",
                "value": "470u",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": true,
                "ordercode": "1835537",
                "posx": 201,
                "posy": 39,
                "price": 1.38,
                "reference": "K9",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "PHONEJACK-STEREO",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "PHONEJACK STEREO",
                "value": "JACK-6M3-F",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 184,
                "posy": 30,
                "price": 0.15,
                "reference": "C5",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 184,
                "posy": 47,
                "price": 0.15,
                "reference": "C6",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 179,
                "posy": 55,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 205,
                "posy": 86,
                "price": 1.2,
                "reference": "D8",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 205,
                "posy": 108,
                "price": 1.2,
                "reference": "D9",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 200,
                "posy": 81,
                "price": 0.04,
                "reference": "R37",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 200,
                "posy": 103,
                "price": 0.04,
                "reference": "R38",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 193,
                "posy": 88,
                "price": 0.15,
                "reference": "C15",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 193,
                "posy": 110,
                "price": 0.15,
                "reference": "C16",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "9353844",
                "posx": 176,
                "posy": 85,
                "price": 2.91,
                "reference": "R35",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X03",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "rvar",
                "value": "ALT-10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "9353844",
                "posx": 176,
                "posy": 107,
                "price": 2.91,
                "reference": "R36",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "RPOT-MIDIATOR",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "rvar",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329515",
                "posx": 171,
                "posy": 86,
                "price": 0.03,
                "reference": "R31",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "33k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329515",
                "posx": 171,
                "posy": 108,
                "price": 0.03,
                "reference": "R32",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "33k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3366024\n",
                "posx": 166,
                "posy": 87,
                "price": 0.99,
                "reference": "C13",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "CTANT-01",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor pol",
                "value": "1u",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3366024\n",
                "posx": 166,
                "posy": 109,
                "price": 0.99,
                "reference": "C14",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "CTANT-01",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor pol",
                "value": "1u",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 160,
                "posy": 88,
                "price": 0.15,
                "reference": "C11",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 160,
                "posy": 109,
                "price": 0.15,
                "reference": "C12",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2675146",
                "posx": 154,
                "posy": 81,
                "price": 0.05,
                "reference": "D6",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIODE-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "diode",
                "value": "1N4148",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2675146",
                "posx": 154,
                "posy": 103,
                "price": 0.05,
                "reference": "D7",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIODE-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "diode",
                "value": "1N4148",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329472",
                "posx": 154,
                "posy": 87,
                "price": 0.03,
                "reference": "R29",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "100k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329472",
                "posx": 154,
                "posy": 109,
                "price": 0.03,
                "reference": "R30",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "100k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 137,
                "posy": 76,
                "price": 0.07,
                "reference": "R25",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 137,
                "posy": 87,
                "price": 0.07,
                "reference": "R27",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 137,
                "posy": 98,
                "price": 0.07,
                "reference": "R26",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 137,
                "posy": 109,
                "price": 0.07,
                "reference": "R28",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329534",
                "posx": 132,
                "posy": 82,
                "price": 0.03,
                "reference": "R23",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "4k7",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329534",
                "posx": 132,
                "posy": 104,
                "price": 0.03,
                "reference": "R24",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "4k7",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 180,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": true,
                "ordercode": "1280896",
                "posx": 117,
                "posy": 81,
                "price": 1.25,
                "reference": "K10",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X03",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "xlr",
                "value": "XLR-F",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 180,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": true,
                "ordercode": "1280896",
                "posx": 117,
                "posy": 103,
                "price": 1.25,
                "reference": "K11",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X03",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "xlr",
                "value": "XLR-F",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1467723",
                "posx": 98,
                "posy": 83,
                "price": 2.05,
                "reference": "IC10",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-08",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "24512",
                "value": "24512",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1467723",
                "posx": 98,
                "posy": 90,
                "price": 2.05,
                "reference": "IC11",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-08",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "24512",
                "value": "24512",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1467723",
                "posx": 98,
                "posy": 97,
                "price": 2.05,
                "reference": "IC12",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-08",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "24512",
                "value": "24512",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1467723",
                "posx": 98,
                "posy": 104,
                "price": 2.05,
                "reference": "IC13",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-08",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "24512",
                "value": "24512",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 162,
                "posy": 72,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 162,
                "posy": 115,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 176,
                "posy": 76,
                "price": 0.07,
                "reference": "R33",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 176,
                "posy": 98,
                "price": 0.07,
                "reference": "R34",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 93,
                "posy": 110,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 92,
                "posy": 74,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 89,
                "posy": 78,
                "price": 0.15,
                "reference": "C17",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 89,
                "posy": 85,
                "price": 0.15,
                "reference": "C18",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 89,
                "posy": 92,
                "price": 0.15,
                "reference": "C19",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 89,
                "posy": 98,
                "price": 0.15,
                "reference": "C20",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 89,
                "posy": 105,
                "price": 0.15,
                "reference": "C21",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 91,
                "posy": 140,
                "price": 0.15,
                "reference": "C22",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 131,
                "posy": 140,
                "price": 0.15,
                "reference": "C24",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3006909",
                "posx": 96,
                "posy": 131,
                "price": 0.44,
                "reference": "IC14",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-08",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "NE555",
                "value": "NE555",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3006909",
                "posx": 136,
                "posy": 131,
                "price": 0.44,
                "reference": "IC15",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-08",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "NE555",
                "value": "NE555",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3006909",
                "posx": 176,
                "posy": 131,
                "price": 0.44,
                "reference": "IC16",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-08",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "NE555",
                "value": "NE555",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 108,
                "posy": 140,
                "price": 0.15,
                "reference": "C23",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 148,
                "posy": 140,
                "price": 0.15,
                "reference": "C25",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 171,
                "posy": 140,
                "price": 0.15,
                "reference": "C26",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329486",
                "posx": 108,
                "posy": 122,
                "price": 0.07,
                "reference": "R39",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "1k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 188,
                "posy": 140,
                "price": 0.15,
                "reference": "C27",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 112,
                "posy": 122,
                "price": 0.07,
                "reference": "R40",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 126,
                "posy": 128,
                "price": 1.2,
                "reference": "D10",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 166,
                "posy": 128,
                "price": 1.2,
                "reference": "D11",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2062420",
                "posx": 206,
                "posy": 128,
                "price": 1.2,
                "reference": "D12",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "LED-3MM",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "LED",
                "value": "RED",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 126,
                "posy": 122,
                "price": 0.04,
                "reference": "R41",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 166,
                "posy": 122,
                "price": 0.04,
                "reference": "R44",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329482",
                "posx": 206,
                "posy": 122,
                "price": 0.04,
                "reference": "R47",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "150",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329486",
                "posx": 148,
                "posy": 122,
                "price": 0.07,
                "reference": "R42",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "1k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 152,
                "posy": 122,
                "price": 0.07,
                "reference": "R43",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329486",
                "posx": 188,
                "posy": 122,
                "price": 0.07,
                "reference": "R45",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "1k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329474",
                "posx": 192,
                "posy": 122,
                "price": 0.07,
                "reference": "R46",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "10k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 104,
                "posy": 118,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 103,
                "posy": 150,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 46,
                "posy": 116,
                "price": 0.15,
                "reference": "C28",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 46,
                "posy": 139,
                "price": 0.15,
                "reference": "C29",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1860613",
                "posx": 23,
                "posy": 144,
                "price": 0.38,
                "reference": "K13",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "USB-A",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "header4",
                "value": "USB-A",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": true,
                "ordercode": "1200125",
                "posx": 6,
                "posy": 145,
                "price": 0.48,
                "reference": "K12",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SCREW-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "header2",
                "value": "SCREW-02",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2762118",
                "posx": 13,
                "posy": 145,
                "price": 1.63,
                "reference": "C30",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "CTANT-01",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor pol",
                "value": "10u",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 15,
                "posy": 151,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "gnd power",
                "value": "GND",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 15,
                "posy": 141,
                "price": 0,
                "reference": "",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "vcc",
                "value": "VCC",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 270,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2675605",
                "posx": 99,
                "posy": 38,
                "price": 10.61,
                "reference": "K7",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-1X16",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "header1x16",
                "value": "LCD-DISPLAY",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "",
                "posx": 137,
                "posy": 37,
                "price": 0,
                "reference": "K8",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "SIL-2X08",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "header2x8",
                "value": "HEADER-2X8",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1146037",
                "posx": 12,
                "posy": 123,
                "price": 20.21,
                "reference": "K6",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "FTDI-MM232R",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "header2x8",
                "value": "FTDI MM232R",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3120490",
                "posx": 138,
                "posy": 27,
                "price": 0.62,
                "reference": "IC9",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "7402",
                "value": "7402",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 1,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3120490",
                "posx": 117,
                "posy": 132,
                "price": 0.62,
                "reference": "IC9",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "7402",
                "value": "7402",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 2,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3120490",
                "posx": 157,
                "posy": 132,
                "price": 0.62,
                "reference": "IC9",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "7402",
                "value": "7402",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 3,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3120490",
                "posx": 197,
                "posy": 132,
                "price": 0.62,
                "reference": "IC9",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "7402",
                "value": "7402",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329486",
                "posx": 192,
                "posy": 28,
                "price": 0.07,
                "reference": "R48",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "1k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2329486",
                "posx": 192,
                "posy": 45,
                "price": 0.07,
                "reference": "R49",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "R-04",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "R",
                "value": "1k",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3366024\n",
                "posx": 127,
                "posy": 82,
                "price": 0.99,
                "reference": "C9",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "CTANT-01",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor pol",
                "value": "1u",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 90,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3366024\n",
                "posx": 127,
                "posy": 104,
                "price": 0.99,
                "reference": "C10",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "CTANT-01",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "capacitor pol",
                "value": "1u",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": true,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1141777",
                "posx": 78,
                "posy": 44,
                "price": 0.15,
                "reference": "C3",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "C-02",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "CAP",
                "value": "100n",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1045432",
                "posx": 15,
                "posy": 73,
                "price": 1.83,
                "reference": "IC2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-16",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "cny74-4",
                "value": "CNY74-4",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 1,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1045432",
                "posx": 25,
                "posy": 73,
                "price": 1.83,
                "reference": "IC2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-16",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "cny74-4",
                "value": "CNY74-4",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 2,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1045432",
                "posx": 35,
                "posy": 73,
                "price": 1.83,
                "reference": "IC2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-16",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "cny74-4",
                "value": "CNY74-4",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 3,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1045432",
                "posx": 45,
                "posy": 73,
                "price": 1.83,
                "reference": "IC2",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-16",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "cny74-4",
                "value": "CNY74-4",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1137304",
                "posx": 98,
                "posy": 76,
                "price": 0.53,
                "reference": "IC6",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "TO-92D",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "mc101",
                "value": "MC101-475DI-TO",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "2575349",
                "posx": 116,
                "posy": 21,
                "price": 0.59,
                "reference": "T1",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "TO-92D",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "bc517",
                "value": "BC517",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1627199",
                "posx": 99,
                "posy": 20,
                "price": 2.91,
                "reference": "IC7",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "mcp6024",
                "value": "MCP6024",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 1,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1627199",
                "posx": 175,
                "posy": 23,
                "price": 2.91,
                "reference": "IC7",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "mcp6024",
                "value": "MCP6024",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 2,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1627199",
                "posx": 175,
                "posy": 40,
                "price": 2.91,
                "reference": "IC7",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "mcp6024",
                "value": "MCP6024",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1627199",
                "posx": 144,
                "posy": 80,
                "price": 2.91,
                "reference": "IC8",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "mcp6024",
                "value": "MCP6024",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 1,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1627199",
                "posx": 184,
                "posy": 80,
                "price": 2.91,
                "reference": "IC8",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "mcp6024",
                "value": "MCP6024",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 2,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1627199",
                "posx": 144,
                "posy": 102,
                "price": 2.91,
                "reference": "IC8",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "mcp6024",
                "value": "MCP6024",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 3,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "1627199",
                "posx": 184,
                "posy": 102,
                "price": 2.91,
                "reference": "IC8",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-14",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "mcp6024",
                "value": "MCP6024",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3006869",
                "posx": 44,
                "posy": 100,
                "price": 1.1,
                "reference": "IC4",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-16",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "74153",
                "value": "74153",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            },
            {
                "angle": 0,
                "converted": false,
                "device": 0,
                "hidePinNumbers": false,
                "hideReference": false,
                "hideShape": true,
                "hideValue": false,
                "mirrored": false,
                "ordercode": "3006869",
                "posx": 44,
                "posy": 123,
                "price": 1.1,
                "reference": "IC5",
                "referenceDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "shape": "DIL-16",
                "shapeDisplacement": {
                    "x": 0,
                    "y": 0
                },
                "symbolname": "74153",
                "value": "74153",
                "valueDisplacement": {
                    "x": 0,
                    "y": 0
                }
            }
        ],
        "junction": [
            {
                "angle": 0,
                "mirrored": false,
                "posx": 5,
                "posy": 16
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 5,
                "posy": 20
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 5,
                "posy": 24
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 5,
                "posy": 28
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 9,
                "posy": 46
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 10,
                "posy": 46
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 20,
                "posy": 51
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 26,
                "posy": 51
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 30,
                "posy": 51
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 32,
                "posy": 51
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 36,
                "posy": 51
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 40,
                "posy": 51
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 46,
                "posy": 51
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 102
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 104
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 108
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 109
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 124
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 125
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 126
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 130
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 35,
                "posy": 129
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 35,
                "posy": 107
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 35,
                "posy": 106
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 35,
                "posy": 103
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 35,
                "posy": 101
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 40,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 40,
                "posy": 136
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 40,
                "posy": 137
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 62,
                "posy": 101
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 79,
                "posy": 48
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 79,
                "posy": 42
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 60,
                "posy": 41
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 61,
                "posy": 44
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 62,
                "posy": 45
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 7,
                "posy": 135
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 14,
                "posy": 135
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 26,
                "posy": 102
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 16,
                "posy": 87
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 18,
                "posy": 87
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 20,
                "posy": 87
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 30,
                "posy": 87
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 40,
                "posy": 87
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 30,
                "posy": 69
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 20,
                "posy": 70
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 91,
                "posy": 10
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 97,
                "posy": 10
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 100,
                "posy": 10
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 107,
                "posy": 10
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 87,
                "posy": 35
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 91,
                "posy": 35
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 95,
                "posy": 35
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 100,
                "posy": 35
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 107,
                "posy": 35
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 102,
                "posy": 46
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 101,
                "posy": 48
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 104,
                "posy": 48
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 114,
                "posy": 45
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 103,
                "posy": 66
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 105,
                "posy": 65
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 106,
                "posy": 63
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 107,
                "posy": 62
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 108,
                "posy": 61
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 109,
                "posy": 60
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 110,
                "posy": 59
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 111,
                "posy": 58
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 112,
                "posy": 57
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 113,
                "posy": 56
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 132,
                "posy": 48
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 125,
                "posy": 45
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 125,
                "posy": 38
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 36,
                "posy": 111
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 37,
                "posy": 112
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 80,
                "posy": 42
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 84,
                "posy": 74
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 79,
                "posy": 81
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 71,
                "posy": 81
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 75,
                "posy": 86
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 118,
                "posy": 28
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 155,
                "posy": 11
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 170,
                "posy": 11
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 176,
                "posy": 11
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 169,
                "posy": 24
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 164,
                "posy": 33
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 176,
                "posy": 33
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 183,
                "posy": 25
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 176,
                "posy": 36
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 183,
                "posy": 42
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 185,
                "posy": 54
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 176,
                "posy": 54
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 180,
                "posy": 54
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 145,
                "posy": 74
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 161,
                "posy": 74
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 164,
                "posy": 74
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 163,
                "posy": 74
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 177,
                "posy": 74
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 185,
                "posy": 74
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 115,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 120,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 138,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 145,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 161,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 167,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 172,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 177,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 185,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 194,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 138,
                "posy": 81
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 140,
                "posy": 83
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 159,
                "posy": 82
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 167,
                "posy": 81
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 172,
                "posy": 81
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 195,
                "posy": 82
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 145,
                "posy": 96
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 161,
                "posy": 96
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 164,
                "posy": 96
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 177,
                "posy": 96
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 185,
                "posy": 96
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 115,
                "posy": 105
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 120,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 138,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 140,
                "posy": 105
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 138,
                "posy": 103
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 145,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 161,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 159,
                "posy": 104
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 167,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 172,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 177,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 163,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 185,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 194,
                "posy": 114
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 167,
                "posy": 103
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 172,
                "posy": 103
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 196,
                "posy": 104
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 77
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 91
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 99
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 105
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 84
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 85
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 86
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 93
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 98
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 100
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 107
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 108,
                "posy": 84
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 109,
                "posy": 85
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 110,
                "posy": 86
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 108,
                "posy": 91
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 109,
                "posy": 92
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 110,
                "posy": 93
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 108,
                "posy": 98
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 109,
                "posy": 99
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 110,
                "posy": 100
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 108
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 103
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 101
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 96
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 95
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 90
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 88
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 83
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 94,
                "posy": 81
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 93,
                "posy": 76
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 36,
                "posy": 134
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 37,
                "posy": 135
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 61,
                "posy": 113
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 60,
                "posy": 135
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 33,
                "posy": 131
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 92,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 101,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 104,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 109,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 132,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 141,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 149,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 172,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 181,
                "posy": 149
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 103,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 105,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 109,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 113,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 127,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 140,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 143,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 149,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 153,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 167,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 180,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 183,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 189,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 193,
                "posy": 120
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 109,
                "posy": 135
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 109,
                "posy": 137
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 113,
                "posy": 133
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 149,
                "posy": 137
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 149,
                "posy": 135
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 153,
                "posy": 133
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 189,
                "posy": 137
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 189,
                "posy": 135
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 192,
                "posy": 133
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 193,
                "posy": 133
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 62,
                "posy": 123
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 62,
                "posy": 117
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 40,
                "posy": 117
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 40,
                "posy": 140
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 16,
                "posy": 150
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 14,
                "posy": 150
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 14,
                "posy": 143
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 16,
                "posy": 143
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 127,
                "posy": 30
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 193,
                "posy": 54
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 193,
                "posy": 42
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 193,
                "posy": 25
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 185,
                "posy": 33
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 40,
                "posy": 67
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 50,
                "posy": 66
            },
            {
                "angle": 0,
                "mirrored": false,
                "posx": 124,
                "posy": 47
            }
        ],
        "sizex": 224,
        "sizey": 166,
        "text": [
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 78,
                "posy": 21,
                "rightAllign": false,
                "value": "BACKLIGHT"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 78,
                "posy": 29,
                "rightAllign": false,
                "value": "CONTRAST"
            },
            {
                "angle": 90,
                "height": 1,
                "mirrored": false,
                "posx": 162,
                "posy": 27,
                "rightAllign": false,
                "value": "VOLUME"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 178,
                "posy": 88,
                "rightAllign": false,
                "value": "KICK LEVEL"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 178,
                "posy": 110,
                "rightAllign": false,
                "value": "SNARE LEVEL"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 22,
                "posy": 61,
                "rightAllign": false,
                "value": "TRG1"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 32,
                "posy": 61,
                "rightAllign": false,
                "value": "TRG2"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 42,
                "posy": 61,
                "rightAllign": false,
                "value": "TRG3"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 52,
                "posy": 61,
                "rightAllign": false,
                "value": "TRG4"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 151,
                "posy": 24,
                "rightAllign": false,
                "value": "BEAT"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 208,
                "posy": 88,
                "rightAllign": false,
                "value": "KICK-DETECT"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 208,
                "posy": 110,
                "rightAllign": false,
                "value": "SNARE-DETECT"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 209,
                "posy": 130,
                "rightAllign": false,
                "value": "MIDI-IN"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 129,
                "posy": 130,
                "rightAllign": false,
                "value": "MIDI-OUT1"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 169,
                "posy": 130,
                "rightAllign": false,
                "value": "MIDI-OUT2"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 117,
                "posy": 78,
                "rightAllign": false,
                "value": "KICK-MIC"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 117,
                "posy": 100,
                "rightAllign": false,
                "value": "SNARE-MIC"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 11,
                "posy": 102,
                "rightAllign": false,
                "value": "MIDI-IN"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 77,
                "posy": 110,
                "rightAllign": false,
                "value": "MIDI-OUT1"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 77,
                "posy": 132,
                "rightAllign": false,
                "value": "MIDI-OUT2"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 5,
                "posy": 74,
                "rightAllign": false,
                "value": "TRIGGER1-2"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 5,
                "posy": 89,
                "rightAllign": false,
                "value": "TRIGGER3-4"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 201,
                "posy": 45,
                "rightAllign": false,
                "value": "HEADPHONES"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 104,
                "posy": 37,
                "rightAllign": false,
                "value": "LCD DISPLAY"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 134,
                "posy": 35,
                "rightAllign": false,
                "value": "ALTERNATIVE LCD DISPLAY"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 71,
                "posy": 48,
                "rightAllign": false,
                "value": "PGM"
            },
            {
                "angle": 90,
                "height": 1,
                "mirrored": false,
                "posx": 27,
                "posy": 151,
                "rightAllign": false,
                "value": "USB-POWER-OUT"
            },
            {
                "angle": 90,
                "height": 1,
                "mirrored": false,
                "posx": 4,
                "posy": 149,
                "rightAllign": false,
                "value": "POWER-IN"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 13,
                "posy": 11,
                "rightAllign": false,
                "value": "PLAY/ENTER"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 13,
                "posy": 15,
                "rightAllign": false,
                "value": "RECORD"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 13,
                "posy": 19,
                "rightAllign": false,
                "value": "STOP/ESCAPE"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 13,
                "posy": 23,
                "rightAllign": false,
                "value": "LEFT"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 13,
                "posy": 27,
                "rightAllign": false,
                "value": "RIGHT"
            },
            {
                "angle": 0,
                "height": 1,
                "mirrored": false,
                "posx": 17,
                "posy": 39,
                "rightAllign": false,
                "value": "SELECT"
            }
        ],
        "title": [
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            },
            {
                "address_line1": "TWISQ - QSCHEME - https://www.twisq.nl",
                "address_line2": "",
                "address_line3": "",
                "address_line4": "",
                "angle": 0,
                "date": "Sun Aug 22 2021",
                "document_number": "TIQ210801",
                "mirrored": false,
                "number_of_sheets": 1,
                "organization_name": "",
                "posx": 0,
                "posy": 0,
                "revision": "1.0",
                "sheet_number": 1,
                "sheet_size": "X",
                "title_of_sheet": "MIDIATOR"
            }
        ],
        "wire": [
            {
                "points": [
                    {
                        "x": 6,
                        "y": 16
                    },
                    {
                        "x": 5,
                        "y": 16
                    },
                    {
                        "x": 5,
                        "y": 16
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 6,
                        "y": 20
                    },
                    {
                        "x": 5,
                        "y": 20
                    },
                    {
                        "x": 5,
                        "y": 20
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 6,
                        "y": 24
                    },
                    {
                        "x": 5,
                        "y": 24
                    },
                    {
                        "x": 5,
                        "y": 24
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 6,
                        "y": 28
                    },
                    {
                        "x": 5,
                        "y": 28
                    },
                    {
                        "x": 5,
                        "y": 28
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 10,
                        "y": 46
                    },
                    {
                        "x": 10,
                        "y": 47
                    },
                    {
                        "x": 10,
                        "y": 47
                    },
                    {
                        "x": 10,
                        "y": 47
                    },
                    {
                        "x": 10,
                        "y": 47
                    },
                    {
                        "x": 10,
                        "y": 47
                    },
                    {
                        "x": 10,
                        "y": 47
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 16,
                        "y": 52
                    },
                    {
                        "x": 16,
                        "y": 51
                    },
                    {
                        "x": 50,
                        "y": 51
                    },
                    {
                        "x": 50,
                        "y": 52
                    },
                    {
                        "x": 50,
                        "y": 52
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 46,
                        "y": 51
                    },
                    {
                        "x": 46,
                        "y": 52
                    },
                    {
                        "x": 46,
                        "y": 52
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 51
                    },
                    {
                        "x": 40,
                        "y": 52
                    },
                    {
                        "x": 40,
                        "y": 52
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 36,
                        "y": 51
                    },
                    {
                        "x": 36,
                        "y": 52
                    },
                    {
                        "x": 36,
                        "y": 52
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 51
                    },
                    {
                        "x": 30,
                        "y": 52
                    },
                    {
                        "x": 30,
                        "y": 52
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 26,
                        "y": 51
                    },
                    {
                        "x": 26,
                        "y": 52
                    },
                    {
                        "x": 26,
                        "y": 52
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 51
                    },
                    {
                        "x": 20,
                        "y": 52
                    },
                    {
                        "x": 20,
                        "y": 52
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 32,
                        "y": 51
                    },
                    {
                        "x": 32,
                        "y": 50
                    },
                    {
                        "x": 32,
                        "y": 50
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 16,
                        "y": 57
                    },
                    {
                        "x": 16,
                        "y": 69
                    },
                    {
                        "x": 16,
                        "y": 69
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 69
                    },
                    {
                        "x": 20,
                        "y": 64
                    },
                    {
                        "x": 20,
                        "y": 64
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 58
                    },
                    {
                        "x": 20,
                        "y": 57
                    },
                    {
                        "x": 20,
                        "y": 57
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 26,
                        "y": 57
                    },
                    {
                        "x": 26,
                        "y": 69
                    },
                    {
                        "x": 26,
                        "y": 69
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 69
                    },
                    {
                        "x": 30,
                        "y": 64
                    },
                    {
                        "x": 30,
                        "y": 64
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 58
                    },
                    {
                        "x": 30,
                        "y": 57
                    },
                    {
                        "x": 30,
                        "y": 57
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 36,
                        "y": 57
                    },
                    {
                        "x": 36,
                        "y": 69
                    },
                    {
                        "x": 36,
                        "y": 69
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 58
                    },
                    {
                        "x": 40,
                        "y": 57
                    },
                    {
                        "x": 40,
                        "y": 57
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 46,
                        "y": 57
                    },
                    {
                        "x": 46,
                        "y": 69
                    },
                    {
                        "x": 46,
                        "y": 69
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 50,
                        "y": 57
                    },
                    {
                        "x": 50,
                        "y": 58
                    },
                    {
                        "x": 50,
                        "y": 58
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 33,
                        "y": 130
                    },
                    {
                        "x": 39,
                        "y": 130
                    },
                    {
                        "x": 39,
                        "y": 130
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 102
                    },
                    {
                        "x": 33,
                        "y": 102
                    },
                    {
                        "x": 33,
                        "y": 102
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 104
                    },
                    {
                        "x": 33,
                        "y": 104
                    },
                    {
                        "x": 33,
                        "y": 104
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 108
                    },
                    {
                        "x": 33,
                        "y": 108
                    },
                    {
                        "x": 33,
                        "y": 108
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 109
                    },
                    {
                        "x": 33,
                        "y": 109
                    },
                    {
                        "x": 33,
                        "y": 109
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 124
                    },
                    {
                        "x": 33,
                        "y": 124
                    },
                    {
                        "x": 33,
                        "y": 124
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 125
                    },
                    {
                        "x": 33,
                        "y": 125
                    },
                    {
                        "x": 33,
                        "y": 125
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 126
                    },
                    {
                        "x": 33,
                        "y": 126
                    },
                    {
                        "x": 33,
                        "y": 126
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 48,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 113
                    },
                    {
                        "x": 40,
                        "y": 113
                    },
                    {
                        "x": 40,
                        "y": 141
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 114
                    },
                    {
                        "x": 41,
                        "y": 114
                    },
                    {
                        "x": 41,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 136
                    },
                    {
                        "x": 41,
                        "y": 136
                    },
                    {
                        "x": 41,
                        "y": 136
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 137
                    },
                    {
                        "x": 41,
                        "y": 137
                    },
                    {
                        "x": 41,
                        "y": 137
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 131
                    },
                    {
                        "x": 41,
                        "y": 131
                    },
                    {
                        "x": 41,
                        "y": 131
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 130
                    },
                    {
                        "x": 41,
                        "y": 130
                    },
                    {
                        "x": 41,
                        "y": 130
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 126
                    },
                    {
                        "x": 41,
                        "y": 126
                    },
                    {
                        "x": 41,
                        "y": 126
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 125
                    },
                    {
                        "x": 41,
                        "y": 125
                    },
                    {
                        "x": 41,
                        "y": 125
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 124
                    },
                    {
                        "x": 41,
                        "y": 124
                    },
                    {
                        "x": 41,
                        "y": 124
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 109
                    },
                    {
                        "x": 40,
                        "y": 109
                    },
                    {
                        "x": 40,
                        "y": 109
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 109
                    },
                    {
                        "x": 41,
                        "y": 109
                    },
                    {
                        "x": 41,
                        "y": 109
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 108
                    },
                    {
                        "x": 41,
                        "y": 108
                    },
                    {
                        "x": 41,
                        "y": 108
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 104
                    },
                    {
                        "x": 41,
                        "y": 104
                    },
                    {
                        "x": 41,
                        "y": 104
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 39,
                        "y": 102
                    },
                    {
                        "x": 41,
                        "y": 102
                    },
                    {
                        "x": 41,
                        "y": 102
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 127
                    },
                    {
                        "x": 34,
                        "y": 127
                    },
                    {
                        "x": 34,
                        "y": 127
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 101
                    },
                    {
                        "x": 35,
                        "y": 101
                    },
                    {
                        "x": 35,
                        "y": 101
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 103
                    },
                    {
                        "x": 35,
                        "y": 103
                    },
                    {
                        "x": 35,
                        "y": 103
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 106
                    },
                    {
                        "x": 35,
                        "y": 106
                    },
                    {
                        "x": 35,
                        "y": 106
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 107
                    },
                    {
                        "x": 35,
                        "y": 107
                    },
                    {
                        "x": 35,
                        "y": 107
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 129
                    },
                    {
                        "x": 35,
                        "y": 129
                    },
                    {
                        "x": 35,
                        "y": 129
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 132
                    },
                    {
                        "x": 35,
                        "y": 132
                    },
                    {
                        "x": 35,
                        "y": 132
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 53,
                        "y": 124
                    },
                    {
                        "x": 59,
                        "y": 124
                    },
                    {
                        "x": 59,
                        "y": 72
                    },
                    {
                        "x": 65,
                        "y": 72
                    },
                    {
                        "x": 65,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 62,
                        "y": 101
                    },
                    {
                        "x": 63,
                        "y": 101
                    },
                    {
                        "x": 63,
                        "y": 101
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 62,
                        "y": 98
                    },
                    {
                        "x": 62,
                        "y": 123
                    },
                    {
                        "x": 63,
                        "y": 123
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 68,
                        "y": 101
                    },
                    {
                        "x": 74,
                        "y": 101
                    },
                    {
                        "x": 74,
                        "y": 101
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 68,
                        "y": 113
                    },
                    {
                        "x": 74,
                        "y": 113
                    },
                    {
                        "x": 74,
                        "y": 113
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 68,
                        "y": 123
                    },
                    {
                        "x": 73,
                        "y": 123
                    },
                    {
                        "x": 73,
                        "y": 123
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 73,
                        "y": 123
                    },
                    {
                        "x": 74,
                        "y": 123
                    },
                    {
                        "x": 74,
                        "y": 123
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 68,
                        "y": 135
                    },
                    {
                        "x": 74,
                        "y": 135
                    },
                    {
                        "x": 74,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 63,
                        "y": 135
                    },
                    {
                        "x": 60,
                        "y": 135
                    },
                    {
                        "x": 60,
                        "y": 106
                    },
                    {
                        "x": 53,
                        "y": 106
                    },
                    {
                        "x": 53,
                        "y": 106
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 63,
                        "y": 113
                    },
                    {
                        "x": 61,
                        "y": 113
                    },
                    {
                        "x": 61,
                        "y": 101
                    },
                    {
                        "x": 53,
                        "y": 101
                    },
                    {
                        "x": 53,
                        "y": 101
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 13,
                        "y": 45
                    },
                    {
                        "x": 13,
                        "y": 46
                    },
                    {
                        "x": 5,
                        "y": 46
                    },
                    {
                        "x": 5,
                        "y": 12
                    },
                    {
                        "x": 6,
                        "y": 12
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 45
                    },
                    {
                        "x": 9,
                        "y": 46
                    },
                    {
                        "x": 9,
                        "y": 46
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 33
                    },
                    {
                        "x": 9,
                        "y": 32
                    },
                    {
                        "x": 9,
                        "y": 32
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 70,
                        "y": 44
                    },
                    {
                        "x": 61,
                        "y": 44
                    },
                    {
                        "x": 61,
                        "y": 44
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 70,
                        "y": 45
                    },
                    {
                        "x": 62,
                        "y": 45
                    },
                    {
                        "x": 62,
                        "y": 45
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 74,
                        "y": 44
                    },
                    {
                        "x": 75,
                        "y": 44
                    },
                    {
                        "x": 75,
                        "y": 42
                    },
                    {
                        "x": 79,
                        "y": 42
                    },
                    {
                        "x": 79,
                        "y": 42
                    },
                    {
                        "x": 79,
                        "y": 43
                    },
                    {
                        "x": 79,
                        "y": 43
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 74,
                        "y": 46
                    },
                    {
                        "x": 75,
                        "y": 46
                    },
                    {
                        "x": 75,
                        "y": 48
                    },
                    {
                        "x": 79,
                        "y": 48
                    },
                    {
                        "x": 79,
                        "y": 46
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 74,
                        "y": 45
                    },
                    {
                        "x": 76,
                        "y": 45
                    },
                    {
                        "x": 76,
                        "y": 45
                    },
                    {
                        "x": 76,
                        "y": 41
                    },
                    {
                        "x": 76,
                        "y": 41
                    },
                    {
                        "x": 60,
                        "y": 41
                    },
                    {
                        "x": 60,
                        "y": 41
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 79,
                        "y": 48
                    },
                    {
                        "x": 79,
                        "y": 49
                    },
                    {
                        "x": 79,
                        "y": 49
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 125
                    },
                    {
                        "x": 8,
                        "y": 125
                    },
                    {
                        "x": 8,
                        "y": 121
                    },
                    {
                        "x": 21,
                        "y": 121
                    },
                    {
                        "x": 21,
                        "y": 124
                    },
                    {
                        "x": 20,
                        "y": 124
                    },
                    {
                        "x": 20,
                        "y": 124
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 131
                    },
                    {
                        "x": 8,
                        "y": 131
                    },
                    {
                        "x": 8,
                        "y": 134
                    },
                    {
                        "x": 21,
                        "y": 134
                    },
                    {
                        "x": 21,
                        "y": 130
                    },
                    {
                        "x": 20,
                        "y": 130
                    },
                    {
                        "x": 20,
                        "y": 130
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 126
                    },
                    {
                        "x": 7,
                        "y": 126
                    },
                    {
                        "x": 7,
                        "y": 120
                    },
                    {
                        "x": 34,
                        "y": 120
                    },
                    {
                        "x": 34,
                        "y": 127
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 127
                    },
                    {
                        "x": 6,
                        "y": 127
                    },
                    {
                        "x": 6,
                        "y": 119
                    },
                    {
                        "x": 54,
                        "y": 119
                    },
                    {
                        "x": 54,
                        "y": 129
                    },
                    {
                        "x": 53,
                        "y": 129
                    },
                    {
                        "x": 53,
                        "y": 129
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 124
                    },
                    {
                        "x": 5,
                        "y": 124
                    },
                    {
                        "x": 5,
                        "y": 135
                    },
                    {
                        "x": 22,
                        "y": 135
                    },
                    {
                        "x": 22,
                        "y": 131
                    },
                    {
                        "x": 20,
                        "y": 131
                    },
                    {
                        "x": 20,
                        "y": 131
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 137
                    },
                    {
                        "x": 14,
                        "y": 135
                    },
                    {
                        "x": 14,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 7,
                        "y": 135
                    },
                    {
                        "x": 7,
                        "y": 130
                    },
                    {
                        "x": 9,
                        "y": 130
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 47
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 99
                    },
                    {
                        "x": 14,
                        "y": 99
                    },
                    {
                        "x": 14,
                        "y": 99
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 19,
                        "y": 99
                    },
                    {
                        "x": 22,
                        "y": 99
                    },
                    {
                        "x": 22,
                        "y": 103
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 22,
                        "y": 107
                    },
                    {
                        "x": 22,
                        "y": 111
                    },
                    {
                        "x": 9,
                        "y": 111
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 26,
                        "y": 103
                    },
                    {
                        "x": 26,
                        "y": 100
                    },
                    {
                        "x": 26,
                        "y": 100
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 26,
                        "y": 95
                    },
                    {
                        "x": 26,
                        "y": 93
                    },
                    {
                        "x": 26,
                        "y": 93
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 26,
                        "y": 102
                    },
                    {
                        "x": 33,
                        "y": 102
                    },
                    {
                        "x": 33,
                        "y": 102
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 33,
                        "y": 102
                    },
                    {
                        "x": 33,
                        "y": 131
                    },
                    {
                        "x": 39,
                        "y": 131
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 26,
                        "y": 107
                    },
                    {
                        "x": 26,
                        "y": 112
                    },
                    {
                        "x": 26,
                        "y": 112
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 77
                    },
                    {
                        "x": 16,
                        "y": 77
                    },
                    {
                        "x": 16,
                        "y": 76
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 79
                    },
                    {
                        "x": 26,
                        "y": 79
                    },
                    {
                        "x": 26,
                        "y": 76
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 84
                    },
                    {
                        "x": 36,
                        "y": 84
                    },
                    {
                        "x": 36,
                        "y": 76
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 86
                    },
                    {
                        "x": 46,
                        "y": 86
                    },
                    {
                        "x": 46,
                        "y": 76
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 50,
                        "y": 76
                    },
                    {
                        "x": 50,
                        "y": 87
                    },
                    {
                        "x": 14,
                        "y": 87
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 76
                    },
                    {
                        "x": 20,
                        "y": 87
                    },
                    {
                        "x": 20,
                        "y": 87
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 76
                    },
                    {
                        "x": 30,
                        "y": 87
                    },
                    {
                        "x": 30,
                        "y": 87
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 76
                    },
                    {
                        "x": 40,
                        "y": 87
                    },
                    {
                        "x": 40,
                        "y": 87
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 80
                    },
                    {
                        "x": 16,
                        "y": 80
                    },
                    {
                        "x": 16,
                        "y": 87
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 18,
                        "y": 87
                    },
                    {
                        "x": 18,
                        "y": 88
                    },
                    {
                        "x": 18,
                        "y": 88
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 16,
                        "y": 69
                    },
                    {
                        "x": 16,
                        "y": 72
                    },
                    {
                        "x": 16,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 69
                    },
                    {
                        "x": 20,
                        "y": 72
                    },
                    {
                        "x": 20,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 26,
                        "y": 69
                    },
                    {
                        "x": 26,
                        "y": 72
                    },
                    {
                        "x": 26,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 69
                    },
                    {
                        "x": 30,
                        "y": 72
                    },
                    {
                        "x": 30,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 36,
                        "y": 69
                    },
                    {
                        "x": 36,
                        "y": 72
                    },
                    {
                        "x": 36,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 46,
                        "y": 69
                    },
                    {
                        "x": 46,
                        "y": 72
                    },
                    {
                        "x": 46,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 87,
                        "y": 26
                    },
                    {
                        "x": 87,
                        "y": 10
                    },
                    {
                        "x": 125,
                        "y": 10
                    },
                    {
                        "x": 125,
                        "y": 45
                    },
                    {
                        "x": 134,
                        "y": 45
                    },
                    {
                        "x": 134,
                        "y": 45
                    },
                    {
                        "x": 134,
                        "y": 45
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 107,
                        "y": 33
                    },
                    {
                        "x": 107,
                        "y": 35
                    },
                    {
                        "x": 107,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 100,
                        "y": 27
                    },
                    {
                        "x": 100,
                        "y": 35
                    },
                    {
                        "x": 100,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 91,
                        "y": 25
                    },
                    {
                        "x": 91,
                        "y": 35
                    },
                    {
                        "x": 91,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 91,
                        "y": 10
                    },
                    {
                        "x": 91,
                        "y": 11
                    },
                    {
                        "x": 91,
                        "y": 11
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 100,
                        "y": 17
                    },
                    {
                        "x": 100,
                        "y": 10
                    },
                    {
                        "x": 100,
                        "y": 10
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 10
                    },
                    {
                        "x": 97,
                        "y": 9
                    },
                    {
                        "x": 97,
                        "y": 9
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 93,
                        "y": 21
                    },
                    {
                        "x": 96,
                        "y": 21
                    },
                    {
                        "x": 96,
                        "y": 21
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 106,
                        "y": 22
                    },
                    {
                        "x": 108,
                        "y": 22
                    },
                    {
                        "x": 108,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 107,
                        "y": 30
                    },
                    {
                        "x": 107,
                        "y": 10
                    },
                    {
                        "x": 107,
                        "y": 10
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 113,
                        "y": 22
                    },
                    {
                        "x": 115,
                        "y": 22
                    },
                    {
                        "x": 115,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 100,
                        "y": 44
                    },
                    {
                        "x": 100,
                        "y": 45
                    },
                    {
                        "x": 125,
                        "y": 45
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 134,
                        "y": 38
                    },
                    {
                        "x": 125,
                        "y": 38
                    },
                    {
                        "x": 125,
                        "y": 38
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 114,
                        "y": 44
                    },
                    {
                        "x": 114,
                        "y": 45
                    },
                    {
                        "x": 114,
                        "y": 45
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 134,
                        "y": 39
                    },
                    {
                        "x": 126,
                        "y": 39
                    },
                    {
                        "x": 126,
                        "y": 46
                    },
                    {
                        "x": 90,
                        "y": 46
                    },
                    {
                        "x": 90,
                        "y": 29
                    },
                    {
                        "x": 89,
                        "y": 29
                    },
                    {
                        "x": 89,
                        "y": 29
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 102,
                        "y": 44
                    },
                    {
                        "x": 102,
                        "y": 46
                    },
                    {
                        "x": 102,
                        "y": 46
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 134,
                        "y": 41
                    },
                    {
                        "x": 128,
                        "y": 41
                    },
                    {
                        "x": 128,
                        "y": 57
                    },
                    {
                        "x": 128,
                        "y": 63
                    },
                    {
                        "x": 83,
                        "y": 63
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 134,
                        "y": 42
                    },
                    {
                        "x": 129,
                        "y": 42
                    },
                    {
                        "x": 129,
                        "y": 61
                    },
                    {
                        "x": 83,
                        "y": 61
                    },
                    {
                        "x": 83,
                        "y": 61
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 134,
                        "y": 43
                    },
                    {
                        "x": 130,
                        "y": 43
                    },
                    {
                        "x": 130,
                        "y": 59
                    },
                    {
                        "x": 83,
                        "y": 59
                    },
                    {
                        "x": 83,
                        "y": 59
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 134,
                        "y": 44
                    },
                    {
                        "x": 131,
                        "y": 44
                    },
                    {
                        "x": 131,
                        "y": 57
                    },
                    {
                        "x": 83,
                        "y": 57
                    },
                    {
                        "x": 83,
                        "y": 57
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 115,
                        "y": 44
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 115,
                        "y": 44
                    },
                    {
                        "x": 115,
                        "y": 47
                    },
                    {
                        "x": 146,
                        "y": 47
                    },
                    {
                        "x": 146,
                        "y": 45
                    },
                    {
                        "x": 145,
                        "y": 45
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 118,
                        "y": 20
                    },
                    {
                        "x": 118,
                        "y": 19
                    },
                    {
                        "x": 124,
                        "y": 19
                    },
                    {
                        "x": 124,
                        "y": 47
                    },
                    {
                        "x": 124,
                        "y": 47
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 38
                    },
                    {
                        "x": 147,
                        "y": 38
                    },
                    {
                        "x": 147,
                        "y": 48
                    },
                    {
                        "x": 87,
                        "y": 48
                    },
                    {
                        "x": 87,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 101,
                        "y": 44
                    },
                    {
                        "x": 101,
                        "y": 48
                    },
                    {
                        "x": 101,
                        "y": 48
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 104,
                        "y": 44
                    },
                    {
                        "x": 104,
                        "y": 48
                    },
                    {
                        "x": 104,
                        "y": 48
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 134,
                        "y": 40
                    },
                    {
                        "x": 132,
                        "y": 40
                    },
                    {
                        "x": 132,
                        "y": 48
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 44
                    },
                    {
                        "x": 148,
                        "y": 44
                    },
                    {
                        "x": 148,
                        "y": 56
                    },
                    {
                        "x": 83,
                        "y": 56
                    },
                    {
                        "x": 83,
                        "y": 56
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 43
                    },
                    {
                        "x": 149,
                        "y": 43
                    },
                    {
                        "x": 149,
                        "y": 58
                    },
                    {
                        "x": 83,
                        "y": 58
                    },
                    {
                        "x": 83,
                        "y": 58
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 42
                    },
                    {
                        "x": 150,
                        "y": 42
                    },
                    {
                        "x": 150,
                        "y": 60
                    },
                    {
                        "x": 83,
                        "y": 60
                    },
                    {
                        "x": 83,
                        "y": 60
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 41
                    },
                    {
                        "x": 151,
                        "y": 41
                    },
                    {
                        "x": 151,
                        "y": 62
                    },
                    {
                        "x": 83,
                        "y": 62
                    },
                    {
                        "x": 83,
                        "y": 62
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 65
                    },
                    {
                        "x": 152,
                        "y": 65
                    },
                    {
                        "x": 152,
                        "y": 40
                    },
                    {
                        "x": 145,
                        "y": 40
                    },
                    {
                        "x": 145,
                        "y": 40
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 39
                    },
                    {
                        "x": 153,
                        "y": 39
                    },
                    {
                        "x": 153,
                        "y": 66
                    },
                    {
                        "x": 83,
                        "y": 66
                    },
                    {
                        "x": 83,
                        "y": 66
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 103,
                        "y": 44
                    },
                    {
                        "x": 103,
                        "y": 66
                    },
                    {
                        "x": 103,
                        "y": 66
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 44
                    },
                    {
                        "x": 105,
                        "y": 65
                    },
                    {
                        "x": 105,
                        "y": 65
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 106,
                        "y": 44
                    },
                    {
                        "x": 106,
                        "y": 63
                    },
                    {
                        "x": 106,
                        "y": 63
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 107,
                        "y": 44
                    },
                    {
                        "x": 107,
                        "y": 62
                    },
                    {
                        "x": 107,
                        "y": 62
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 108,
                        "y": 44
                    },
                    {
                        "x": 108,
                        "y": 61
                    },
                    {
                        "x": 109,
                        "y": 61
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 109,
                        "y": 44
                    },
                    {
                        "x": 109,
                        "y": 60
                    },
                    {
                        "x": 110,
                        "y": 60
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 110,
                        "y": 44
                    },
                    {
                        "x": 110,
                        "y": 59
                    },
                    {
                        "x": 110,
                        "y": 59
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 111,
                        "y": 44
                    },
                    {
                        "x": 111,
                        "y": 58
                    },
                    {
                        "x": 111,
                        "y": 58
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 112,
                        "y": 44
                    },
                    {
                        "x": 112,
                        "y": 57
                    },
                    {
                        "x": 112,
                        "y": 57
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 113,
                        "y": 44
                    },
                    {
                        "x": 113,
                        "y": 56
                    },
                    {
                        "x": 113,
                        "y": 56
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 95,
                        "y": 36
                    },
                    {
                        "x": 95,
                        "y": 35
                    },
                    {
                        "x": 95,
                        "y": 35
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 65
                    },
                    {
                        "x": 66,
                        "y": 65
                    },
                    {
                        "x": 66,
                        "y": 53
                    },
                    {
                        "x": 155,
                        "y": 53
                    },
                    {
                        "x": 155,
                        "y": 53
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 56
                    },
                    {
                        "x": 62,
                        "y": 56
                    },
                    {
                        "x": 62,
                        "y": 34
                    },
                    {
                        "x": 62,
                        "y": 12
                    },
                    {
                        "x": 15,
                        "y": 12
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 57
                    },
                    {
                        "x": 61,
                        "y": 57
                    },
                    {
                        "x": 61,
                        "y": 30
                    },
                    {
                        "x": 61,
                        "y": 30
                    },
                    {
                        "x": 61,
                        "y": 16
                    },
                    {
                        "x": 15,
                        "y": 16
                    },
                    {
                        "x": 15,
                        "y": 16
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 58
                    },
                    {
                        "x": 60,
                        "y": 58
                    },
                    {
                        "x": 60,
                        "y": 20
                    },
                    {
                        "x": 15,
                        "y": 20
                    },
                    {
                        "x": 15,
                        "y": 20
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 59
                    },
                    {
                        "x": 59,
                        "y": 59
                    },
                    {
                        "x": 59,
                        "y": 24
                    },
                    {
                        "x": 15,
                        "y": 24
                    },
                    {
                        "x": 15,
                        "y": 24
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 15,
                        "y": 28
                    },
                    {
                        "x": 58,
                        "y": 28
                    },
                    {
                        "x": 58,
                        "y": 60
                    },
                    {
                        "x": 67,
                        "y": 60
                    },
                    {
                        "x": 67,
                        "y": 60
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 32
                    },
                    {
                        "x": 9,
                        "y": 30
                    },
                    {
                        "x": 57,
                        "y": 30
                    },
                    {
                        "x": 57,
                        "y": 61
                    },
                    {
                        "x": 67,
                        "y": 61
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 12,
                        "y": 33
                    },
                    {
                        "x": 12,
                        "y": 31
                    },
                    {
                        "x": 56,
                        "y": 31
                    },
                    {
                        "x": 56,
                        "y": 62
                    },
                    {
                        "x": 67,
                        "y": 62
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 33
                    },
                    {
                        "x": 14,
                        "y": 33
                    },
                    {
                        "x": 14,
                        "y": 32
                    },
                    {
                        "x": 55,
                        "y": 32
                    },
                    {
                        "x": 55,
                        "y": 63
                    },
                    {
                        "x": 67,
                        "y": 63
                    },
                    {
                        "x": 67,
                        "y": 63
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 55
                    },
                    {
                        "x": 63,
                        "y": 55
                    },
                    {
                        "x": 63,
                        "y": 46
                    },
                    {
                        "x": 70,
                        "y": 46
                    },
                    {
                        "x": 70,
                        "y": 46
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 30,
                        "y": 69
                    },
                    {
                        "x": 67,
                        "y": 69
                    },
                    {
                        "x": 67,
                        "y": 69
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 20,
                        "y": 70
                    },
                    {
                        "x": 67,
                        "y": 70
                    },
                    {
                        "x": 67,
                        "y": 70
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 65,
                        "y": 72
                    },
                    {
                        "x": 67,
                        "y": 72
                    },
                    {
                        "x": 67,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 35,
                        "y": 132
                    },
                    {
                        "x": 35,
                        "y": 90
                    },
                    {
                        "x": 58,
                        "y": 90
                    },
                    {
                        "x": 58,
                        "y": 71
                    },
                    {
                        "x": 67,
                        "y": 71
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 37,
                        "y": 108
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 70
                    },
                    {
                        "x": 87,
                        "y": 70
                    },
                    {
                        "x": 87,
                        "y": 89
                    },
                    {
                        "x": 87,
                        "y": 91
                    },
                    {
                        "x": 36,
                        "y": 91
                    },
                    {
                        "x": 36,
                        "y": 134
                    },
                    {
                        "x": 41,
                        "y": 134
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 41,
                        "y": 111
                    },
                    {
                        "x": 36,
                        "y": 111
                    },
                    {
                        "x": 36,
                        "y": 111
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 69
                    },
                    {
                        "x": 88,
                        "y": 69
                    },
                    {
                        "x": 88,
                        "y": 92
                    },
                    {
                        "x": 37,
                        "y": 92
                    },
                    {
                        "x": 37,
                        "y": 135
                    },
                    {
                        "x": 41,
                        "y": 135
                    },
                    {
                        "x": 41,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 37,
                        "y": 112
                    },
                    {
                        "x": 41,
                        "y": 112
                    },
                    {
                        "x": 41,
                        "y": 112
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 75
                    },
                    {
                        "x": 84,
                        "y": 75
                    },
                    {
                        "x": 84,
                        "y": 42
                    },
                    {
                        "x": 79,
                        "y": 42
                    },
                    {
                        "x": 79,
                        "y": 42
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 74
                    },
                    {
                        "x": 84,
                        "y": 74
                    },
                    {
                        "x": 84,
                        "y": 74
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 80,
                        "y": 41
                    },
                    {
                        "x": 80,
                        "y": 42
                    },
                    {
                        "x": 80,
                        "y": 42
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 75
                    },
                    {
                        "x": 66,
                        "y": 75
                    },
                    {
                        "x": 66,
                        "y": 78
                    },
                    {
                        "x": 79,
                        "y": 78
                    },
                    {
                        "x": 79,
                        "y": 82
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 74
                    },
                    {
                        "x": 65,
                        "y": 74
                    },
                    {
                        "x": 65,
                        "y": 79
                    },
                    {
                        "x": 71,
                        "y": 79
                    },
                    {
                        "x": 71,
                        "y": 82
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 85
                    },
                    {
                        "x": 71,
                        "y": 86
                    },
                    {
                        "x": 79,
                        "y": 86
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 79,
                        "y": 85
                    },
                    {
                        "x": 79,
                        "y": 86
                    },
                    {
                        "x": 79,
                        "y": 86
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 75,
                        "y": 87
                    },
                    {
                        "x": 75,
                        "y": 86
                    },
                    {
                        "x": 75,
                        "y": 86
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 71,
                        "y": 81
                    },
                    {
                        "x": 73,
                        "y": 81
                    },
                    {
                        "x": 73,
                        "y": 81
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 77,
                        "y": 81
                    },
                    {
                        "x": 79,
                        "y": 81
                    },
                    {
                        "x": 79,
                        "y": 81
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 155,
                        "y": 9
                    },
                    {
                        "x": 155,
                        "y": 13
                    },
                    {
                        "x": 155,
                        "y": 13
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 155,
                        "y": 18
                    },
                    {
                        "x": 155,
                        "y": 22
                    },
                    {
                        "x": 155,
                        "y": 22
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 96,
                        "y": 23
                    },
                    {
                        "x": 95,
                        "y": 23
                    },
                    {
                        "x": 95,
                        "y": 28
                    },
                    {
                        "x": 118,
                        "y": 28
                    },
                    {
                        "x": 118,
                        "y": 28
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 118,
                        "y": 24
                    },
                    {
                        "x": 118,
                        "y": 29
                    },
                    {
                        "x": 118,
                        "y": 29
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 118,
                        "y": 34
                    },
                    {
                        "x": 118,
                        "y": 35
                    },
                    {
                        "x": 87,
                        "y": 35
                    },
                    {
                        "x": 87,
                        "y": 33
                    },
                    {
                        "x": 87,
                        "y": 33
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 155,
                        "y": 53
                    },
                    {
                        "x": 160,
                        "y": 53
                    },
                    {
                        "x": 160,
                        "y": 12
                    },
                    {
                        "x": 164,
                        "y": 12
                    },
                    {
                        "x": 164,
                        "y": 13
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 164,
                        "y": 18
                    },
                    {
                        "x": 164,
                        "y": 21
                    },
                    {
                        "x": 164,
                        "y": 21
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 176,
                        "y": 47
                    },
                    {
                        "x": 176,
                        "y": 54
                    },
                    {
                        "x": 176,
                        "y": 54
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 166,
                        "y": 24
                    },
                    {
                        "x": 172,
                        "y": 24
                    },
                    {
                        "x": 172,
                        "y": 24
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 182,
                        "y": 25
                    },
                    {
                        "x": 187,
                        "y": 25
                    },
                    {
                        "x": 187,
                        "y": 25
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 182,
                        "y": 42
                    },
                    {
                        "x": 187,
                        "y": 42
                    },
                    {
                        "x": 187,
                        "y": 42
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 191,
                        "y": 42
                    },
                    {
                        "x": 198,
                        "y": 42
                    },
                    {
                        "x": 198,
                        "y": 42
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 191,
                        "y": 25
                    },
                    {
                        "x": 197,
                        "y": 25
                    },
                    {
                        "x": 197,
                        "y": 40
                    },
                    {
                        "x": 198,
                        "y": 40
                    },
                    {
                        "x": 198,
                        "y": 40
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 176,
                        "y": 20
                    },
                    {
                        "x": 176,
                        "y": 11
                    },
                    {
                        "x": 155,
                        "y": 11
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 198,
                        "y": 43
                    },
                    {
                        "x": 198,
                        "y": 43
                    },
                    {
                        "x": 197,
                        "y": 43
                    },
                    {
                        "x": 197,
                        "y": 54
                    },
                    {
                        "x": 166,
                        "y": 54
                    },
                    {
                        "x": 164,
                        "y": 54
                    },
                    {
                        "x": 164,
                        "y": 28
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 26
                    },
                    {
                        "x": 171,
                        "y": 26
                    },
                    {
                        "x": 171,
                        "y": 31
                    },
                    {
                        "x": 183,
                        "y": 31
                    },
                    {
                        "x": 183,
                        "y": 25
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 43
                    },
                    {
                        "x": 171,
                        "y": 43
                    },
                    {
                        "x": 171,
                        "y": 48
                    },
                    {
                        "x": 183,
                        "y": 48
                    },
                    {
                        "x": 183,
                        "y": 42
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 41
                    },
                    {
                        "x": 169,
                        "y": 41
                    },
                    {
                        "x": 169,
                        "y": 24
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 176,
                        "y": 37
                    },
                    {
                        "x": 176,
                        "y": 36
                    },
                    {
                        "x": 170,
                        "y": 36
                    },
                    {
                        "x": 170,
                        "y": 11
                    },
                    {
                        "x": 170,
                        "y": 11
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 49
                    },
                    {
                        "x": 185,
                        "y": 54
                    },
                    {
                        "x": 185,
                        "y": 54
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 46
                    },
                    {
                        "x": 185,
                        "y": 36
                    },
                    {
                        "x": 176,
                        "y": 36
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 29
                    },
                    {
                        "x": 185,
                        "y": 11
                    },
                    {
                        "x": 176,
                        "y": 11
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 32
                    },
                    {
                        "x": 185,
                        "y": 33
                    },
                    {
                        "x": 164,
                        "y": 33
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 176,
                        "y": 30
                    },
                    {
                        "x": 176,
                        "y": 33
                    },
                    {
                        "x": 176,
                        "y": 33
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 180,
                        "y": 54
                    },
                    {
                        "x": 180,
                        "y": 55
                    },
                    {
                        "x": 180,
                        "y": 55
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 116,
                        "y": 83
                    },
                    {
                        "x": 115,
                        "y": 83
                    },
                    {
                        "x": 115,
                        "y": 112
                    },
                    {
                        "x": 115,
                        "y": 114
                    },
                    {
                        "x": 206,
                        "y": 114
                    },
                    {
                        "x": 206,
                        "y": 113
                    },
                    {
                        "x": 206,
                        "y": 113
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 138,
                        "y": 75
                    },
                    {
                        "x": 138,
                        "y": 75
                    },
                    {
                        "x": 138,
                        "y": 74
                    },
                    {
                        "x": 194,
                        "y": 74
                    },
                    {
                        "x": 194,
                        "y": 87
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 77
                    },
                    {
                        "x": 145,
                        "y": 74
                    },
                    {
                        "x": 145,
                        "y": 74
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 163,
                        "y": 74
                    },
                    {
                        "x": 163,
                        "y": 73
                    },
                    {
                        "x": 163,
                        "y": 73
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 77
                    },
                    {
                        "x": 185,
                        "y": 74
                    },
                    {
                        "x": 185,
                        "y": 74
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 206,
                        "y": 91
                    },
                    {
                        "x": 206,
                        "y": 92
                    },
                    {
                        "x": 115,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 120,
                        "y": 88
                    },
                    {
                        "x": 120,
                        "y": 92
                    },
                    {
                        "x": 120,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 116,
                        "y": 105
                    },
                    {
                        "x": 115,
                        "y": 105
                    },
                    {
                        "x": 115,
                        "y": 105
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 120,
                        "y": 110
                    },
                    {
                        "x": 120,
                        "y": 114
                    },
                    {
                        "x": 120,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 138,
                        "y": 113
                    },
                    {
                        "x": 138,
                        "y": 114
                    },
                    {
                        "x": 138,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 109
                    },
                    {
                        "x": 145,
                        "y": 114
                    },
                    {
                        "x": 145,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 163,
                        "y": 114
                    },
                    {
                        "x": 163,
                        "y": 115
                    },
                    {
                        "x": 163,
                        "y": 115
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 124,
                        "y": 83
                    },
                    {
                        "x": 126,
                        "y": 83
                    },
                    {
                        "x": 126,
                        "y": 83
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 136,
                        "y": 83
                    },
                    {
                        "x": 141,
                        "y": 83
                    },
                    {
                        "x": 141,
                        "y": 83
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 138,
                        "y": 80
                    },
                    {
                        "x": 138,
                        "y": 86
                    },
                    {
                        "x": 138,
                        "y": 86
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 138,
                        "y": 91
                    },
                    {
                        "x": 138,
                        "y": 92
                    },
                    {
                        "x": 138,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 141,
                        "y": 81
                    },
                    {
                        "x": 138,
                        "y": 81
                    },
                    {
                        "x": 138,
                        "y": 81
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 87
                    },
                    {
                        "x": 145,
                        "y": 92
                    },
                    {
                        "x": 145,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 151,
                        "y": 82
                    },
                    {
                        "x": 153,
                        "y": 82
                    },
                    {
                        "x": 153,
                        "y": 82
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 158,
                        "y": 88
                    },
                    {
                        "x": 159,
                        "y": 88
                    },
                    {
                        "x": 159,
                        "y": 81
                    },
                    {
                        "x": 181,
                        "y": 81
                    },
                    {
                        "x": 181,
                        "y": 81
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 158,
                        "y": 82
                    },
                    {
                        "x": 159,
                        "y": 82
                    },
                    {
                        "x": 159,
                        "y": 82
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 153,
                        "y": 88
                    },
                    {
                        "x": 140,
                        "y": 88
                    },
                    {
                        "x": 140,
                        "y": 83
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 161,
                        "y": 87
                    },
                    {
                        "x": 161,
                        "y": 74
                    },
                    {
                        "x": 161,
                        "y": 74
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 161,
                        "y": 90
                    },
                    {
                        "x": 161,
                        "y": 92
                    },
                    {
                        "x": 161,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 167,
                        "y": 86
                    },
                    {
                        "x": 167,
                        "y": 81
                    },
                    {
                        "x": 167,
                        "y": 81
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 167,
                        "y": 90
                    },
                    {
                        "x": 167,
                        "y": 92
                    },
                    {
                        "x": 167,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 90
                    },
                    {
                        "x": 172,
                        "y": 92
                    },
                    {
                        "x": 172,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 85
                    },
                    {
                        "x": 172,
                        "y": 81
                    },
                    {
                        "x": 172,
                        "y": 81
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 177,
                        "y": 91
                    },
                    {
                        "x": 177,
                        "y": 92
                    },
                    {
                        "x": 177,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 179,
                        "y": 87
                    },
                    {
                        "x": 180,
                        "y": 87
                    },
                    {
                        "x": 180,
                        "y": 83
                    },
                    {
                        "x": 181,
                        "y": 83
                    },
                    {
                        "x": 181,
                        "y": 83
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 87
                    },
                    {
                        "x": 185,
                        "y": 92
                    },
                    {
                        "x": 185,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 194,
                        "y": 90
                    },
                    {
                        "x": 194,
                        "y": 92
                    },
                    {
                        "x": 194,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 191,
                        "y": 82
                    },
                    {
                        "x": 199,
                        "y": 82
                    },
                    {
                        "x": 199,
                        "y": 82
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 204,
                        "y": 82
                    },
                    {
                        "x": 206,
                        "y": 82
                    },
                    {
                        "x": 206,
                        "y": 85
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 131,
                        "y": 106
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 177,
                        "y": 84
                    },
                    {
                        "x": 177,
                        "y": 80
                    },
                    {
                        "x": 177,
                        "y": 80
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 177,
                        "y": 75
                    },
                    {
                        "x": 177,
                        "y": 74
                    },
                    {
                        "x": 177,
                        "y": 74
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 194,
                        "y": 109
                    },
                    {
                        "x": 194,
                        "y": 96
                    },
                    {
                        "x": 194,
                        "y": 96
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 99
                    },
                    {
                        "x": 185,
                        "y": 96
                    },
                    {
                        "x": 185,
                        "y": 96
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 177,
                        "y": 97
                    },
                    {
                        "x": 177,
                        "y": 96
                    },
                    {
                        "x": 177,
                        "y": 96
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 145,
                        "y": 99
                    },
                    {
                        "x": 145,
                        "y": 96
                    },
                    {
                        "x": 145,
                        "y": 96
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 124,
                        "y": 105
                    },
                    {
                        "x": 126,
                        "y": 105
                    },
                    {
                        "x": 126,
                        "y": 105
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 136,
                        "y": 105
                    },
                    {
                        "x": 141,
                        "y": 105
                    },
                    {
                        "x": 141,
                        "y": 105
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 138,
                        "y": 108
                    },
                    {
                        "x": 138,
                        "y": 102
                    },
                    {
                        "x": 138,
                        "y": 102
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 141,
                        "y": 103
                    },
                    {
                        "x": 138,
                        "y": 103
                    },
                    {
                        "x": 138,
                        "y": 103
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 151,
                        "y": 104
                    },
                    {
                        "x": 153,
                        "y": 104
                    },
                    {
                        "x": 153,
                        "y": 104
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 158,
                        "y": 104
                    },
                    {
                        "x": 159,
                        "y": 104
                    },
                    {
                        "x": 159,
                        "y": 104
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 153,
                        "y": 110
                    },
                    {
                        "x": 140,
                        "y": 110
                    },
                    {
                        "x": 140,
                        "y": 105
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 161,
                        "y": 111
                    },
                    {
                        "x": 161,
                        "y": 114
                    },
                    {
                        "x": 161,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 161,
                        "y": 108
                    },
                    {
                        "x": 161,
                        "y": 96
                    },
                    {
                        "x": 161,
                        "y": 96
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 167,
                        "y": 112
                    },
                    {
                        "x": 167,
                        "y": 114
                    },
                    {
                        "x": 167,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 167,
                        "y": 108
                    },
                    {
                        "x": 167,
                        "y": 103
                    },
                    {
                        "x": 167,
                        "y": 103
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 112
                    },
                    {
                        "x": 172,
                        "y": 114
                    },
                    {
                        "x": 172,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 107
                    },
                    {
                        "x": 172,
                        "y": 103
                    },
                    {
                        "x": 172,
                        "y": 103
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 177,
                        "y": 113
                    },
                    {
                        "x": 177,
                        "y": 114
                    },
                    {
                        "x": 177,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 177,
                        "y": 106
                    },
                    {
                        "x": 177,
                        "y": 102
                    },
                    {
                        "x": 177,
                        "y": 102
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 179,
                        "y": 109
                    },
                    {
                        "x": 180,
                        "y": 109
                    },
                    {
                        "x": 180,
                        "y": 105
                    },
                    {
                        "x": 181,
                        "y": 105
                    },
                    {
                        "x": 181,
                        "y": 105
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 109
                    },
                    {
                        "x": 185,
                        "y": 114
                    },
                    {
                        "x": 185,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 194,
                        "y": 112
                    },
                    {
                        "x": 194,
                        "y": 114
                    },
                    {
                        "x": 194,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 199,
                        "y": 104
                    },
                    {
                        "x": 191,
                        "y": 104
                    },
                    {
                        "x": 191,
                        "y": 104
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 194,
                        "y": 96
                    },
                    {
                        "x": 138,
                        "y": 96
                    },
                    {
                        "x": 138,
                        "y": 97
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 164,
                        "y": 74
                    },
                    {
                        "x": 164,
                        "y": 96
                    },
                    {
                        "x": 164,
                        "y": 96
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 195,
                        "y": 82
                    },
                    {
                        "x": 195,
                        "y": 68
                    },
                    {
                        "x": 83,
                        "y": 68
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 67
                    },
                    {
                        "x": 196,
                        "y": 67
                    },
                    {
                        "x": 196,
                        "y": 104
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 158,
                        "y": 110
                    },
                    {
                        "x": 159,
                        "y": 110
                    },
                    {
                        "x": 159,
                        "y": 103
                    },
                    {
                        "x": 181,
                        "y": 103
                    },
                    {
                        "x": 181,
                        "y": 103
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 72
                    },
                    {
                        "x": 109,
                        "y": 72
                    },
                    {
                        "x": 109,
                        "y": 106
                    },
                    {
                        "x": 105,
                        "y": 106
                    },
                    {
                        "x": 105,
                        "y": 106
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 83,
                        "y": 71
                    },
                    {
                        "x": 110,
                        "y": 71
                    },
                    {
                        "x": 110,
                        "y": 107
                    },
                    {
                        "x": 105,
                        "y": 107
                    },
                    {
                        "x": 105,
                        "y": 107
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 100
                    },
                    {
                        "x": 110,
                        "y": 100
                    },
                    {
                        "x": 110,
                        "y": 100
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 99
                    },
                    {
                        "x": 109,
                        "y": 99
                    },
                    {
                        "x": 109,
                        "y": 99
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 93
                    },
                    {
                        "x": 110,
                        "y": 93
                    },
                    {
                        "x": 110,
                        "y": 93
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 92
                    },
                    {
                        "x": 109,
                        "y": 92
                    },
                    {
                        "x": 109,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 86
                    },
                    {
                        "x": 110,
                        "y": 86
                    },
                    {
                        "x": 110,
                        "y": 86
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 85
                    },
                    {
                        "x": 109,
                        "y": 85
                    },
                    {
                        "x": 109,
                        "y": 85
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 105
                    },
                    {
                        "x": 108,
                        "y": 105
                    },
                    {
                        "x": 108,
                        "y": 78
                    },
                    {
                        "x": 105,
                        "y": 78
                    },
                    {
                        "x": 105,
                        "y": 78
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 84
                    },
                    {
                        "x": 108,
                        "y": 84
                    },
                    {
                        "x": 108,
                        "y": 84
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 91
                    },
                    {
                        "x": 108,
                        "y": 91
                    },
                    {
                        "x": 108,
                        "y": 91
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 98
                    },
                    {
                        "x": 108,
                        "y": 98
                    },
                    {
                        "x": 108,
                        "y": 98
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 79
                    },
                    {
                        "x": 94,
                        "y": 79
                    },
                    {
                        "x": 94,
                        "y": 110
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 77
                    },
                    {
                        "x": 93,
                        "y": 77
                    },
                    {
                        "x": 93,
                        "y": 75
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 84
                    },
                    {
                        "x": 94,
                        "y": 84
                    },
                    {
                        "x": 94,
                        "y": 84
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 91
                    },
                    {
                        "x": 93,
                        "y": 91
                    },
                    {
                        "x": 93,
                        "y": 77
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 98
                    },
                    {
                        "x": 94,
                        "y": 98
                    },
                    {
                        "x": 94,
                        "y": 98
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 105
                    },
                    {
                        "x": 93,
                        "y": 105
                    },
                    {
                        "x": 93,
                        "y": 91
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 85
                    },
                    {
                        "x": 94,
                        "y": 85
                    },
                    {
                        "x": 94,
                        "y": 85
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 92
                    },
                    {
                        "x": 94,
                        "y": 92
                    },
                    {
                        "x": 94,
                        "y": 92
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 99
                    },
                    {
                        "x": 93,
                        "y": 99
                    },
                    {
                        "x": 93,
                        "y": 99
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 106
                    },
                    {
                        "x": 93,
                        "y": 106
                    },
                    {
                        "x": 93,
                        "y": 105
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 86
                    },
                    {
                        "x": 94,
                        "y": 86
                    },
                    {
                        "x": 94,
                        "y": 86
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 93
                    },
                    {
                        "x": 94,
                        "y": 93
                    },
                    {
                        "x": 94,
                        "y": 93
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 100
                    },
                    {
                        "x": 94,
                        "y": 100
                    },
                    {
                        "x": 94,
                        "y": 100
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 97,
                        "y": 107
                    },
                    {
                        "x": 94,
                        "y": 107
                    },
                    {
                        "x": 94,
                        "y": 107
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 100,
                        "y": 114
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 77
                    },
                    {
                        "x": 90,
                        "y": 76
                    },
                    {
                        "x": 93,
                        "y": 76
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 80
                    },
                    {
                        "x": 90,
                        "y": 81
                    },
                    {
                        "x": 94,
                        "y": 81
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 84
                    },
                    {
                        "x": 90,
                        "y": 83
                    },
                    {
                        "x": 93,
                        "y": 83
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 87
                    },
                    {
                        "x": 90,
                        "y": 88
                    },
                    {
                        "x": 94,
                        "y": 88
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 91
                    },
                    {
                        "x": 90,
                        "y": 90
                    },
                    {
                        "x": 93,
                        "y": 90
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 94
                    },
                    {
                        "x": 90,
                        "y": 95
                    },
                    {
                        "x": 94,
                        "y": 95
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 97
                    },
                    {
                        "x": 90,
                        "y": 96
                    },
                    {
                        "x": 93,
                        "y": 96
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 100
                    },
                    {
                        "x": 90,
                        "y": 101
                    },
                    {
                        "x": 94,
                        "y": 101
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 104
                    },
                    {
                        "x": 90,
                        "y": 104
                    },
                    {
                        "x": 90,
                        "y": 103
                    },
                    {
                        "x": 93,
                        "y": 103
                    },
                    {
                        "x": 93,
                        "y": 103
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 90,
                        "y": 107
                    },
                    {
                        "x": 90,
                        "y": 108
                    },
                    {
                        "x": 94,
                        "y": 108
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 207,
                        "y": 121
                    },
                    {
                        "x": 207,
                        "y": 120
                    },
                    {
                        "x": 100,
                        "y": 120
                    },
                    {
                        "x": 100,
                        "y": 128
                    },
                    {
                        "x": 100,
                        "y": 128
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 103,
                        "y": 128
                    },
                    {
                        "x": 103,
                        "y": 120
                    },
                    {
                        "x": 103,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 109,
                        "y": 121
                    },
                    {
                        "x": 109,
                        "y": 120
                    },
                    {
                        "x": 109,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 113,
                        "y": 121
                    },
                    {
                        "x": 113,
                        "y": 120
                    },
                    {
                        "x": 113,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 127,
                        "y": 121
                    },
                    {
                        "x": 127,
                        "y": 120
                    },
                    {
                        "x": 127,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 140,
                        "y": 128
                    },
                    {
                        "x": 140,
                        "y": 120
                    },
                    {
                        "x": 140,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 143,
                        "y": 128
                    },
                    {
                        "x": 143,
                        "y": 121
                    },
                    {
                        "x": 143,
                        "y": 121
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 143,
                        "y": 121
                    },
                    {
                        "x": 143,
                        "y": 120
                    },
                    {
                        "x": 143,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 149,
                        "y": 121
                    },
                    {
                        "x": 149,
                        "y": 120
                    },
                    {
                        "x": 149,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 153,
                        "y": 121
                    },
                    {
                        "x": 153,
                        "y": 120
                    },
                    {
                        "x": 153,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 167,
                        "y": 121
                    },
                    {
                        "x": 167,
                        "y": 120
                    },
                    {
                        "x": 167,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 180,
                        "y": 128
                    },
                    {
                        "x": 180,
                        "y": 120
                    },
                    {
                        "x": 180,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 183,
                        "y": 128
                    },
                    {
                        "x": 183,
                        "y": 120
                    },
                    {
                        "x": 183,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 189,
                        "y": 121
                    },
                    {
                        "x": 189,
                        "y": 120
                    },
                    {
                        "x": 189,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 193,
                        "y": 121
                    },
                    {
                        "x": 193,
                        "y": 120
                    },
                    {
                        "x": 193,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 127,
                        "y": 127
                    },
                    {
                        "x": 127,
                        "y": 126
                    },
                    {
                        "x": 127,
                        "y": 126
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 167,
                        "y": 127
                    },
                    {
                        "x": 167,
                        "y": 126
                    },
                    {
                        "x": 167,
                        "y": 126
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 207,
                        "y": 127
                    },
                    {
                        "x": 207,
                        "y": 126
                    },
                    {
                        "x": 207,
                        "y": 126
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 189,
                        "y": 142
                    },
                    {
                        "x": 189,
                        "y": 149
                    },
                    {
                        "x": 92,
                        "y": 149
                    },
                    {
                        "x": 92,
                        "y": 142
                    },
                    {
                        "x": 92,
                        "y": 142
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 101,
                        "y": 142
                    },
                    {
                        "x": 101,
                        "y": 148
                    },
                    {
                        "x": 101,
                        "y": 148
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 101,
                        "y": 148
                    },
                    {
                        "x": 101,
                        "y": 149
                    },
                    {
                        "x": 101,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 109,
                        "y": 142
                    },
                    {
                        "x": 109,
                        "y": 149
                    },
                    {
                        "x": 109,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 132,
                        "y": 142
                    },
                    {
                        "x": 132,
                        "y": 149
                    },
                    {
                        "x": 132,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 149,
                        "y": 142
                    },
                    {
                        "x": 149,
                        "y": 149
                    },
                    {
                        "x": 149,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 142
                    },
                    {
                        "x": 172,
                        "y": 149
                    },
                    {
                        "x": 172,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 181,
                        "y": 142
                    },
                    {
                        "x": 181,
                        "y": 149
                    },
                    {
                        "x": 181,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 92,
                        "y": 139
                    },
                    {
                        "x": 92,
                        "y": 137
                    },
                    {
                        "x": 93,
                        "y": 137
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 132,
                        "y": 139
                    },
                    {
                        "x": 132,
                        "y": 137
                    },
                    {
                        "x": 133,
                        "y": 137
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 172,
                        "y": 139
                    },
                    {
                        "x": 172,
                        "y": 137
                    },
                    {
                        "x": 173,
                        "y": 137
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 109,
                        "y": 126
                    },
                    {
                        "x": 109,
                        "y": 139
                    },
                    {
                        "x": 109,
                        "y": 139
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 149,
                        "y": 126
                    },
                    {
                        "x": 149,
                        "y": 139
                    },
                    {
                        "x": 149,
                        "y": 139
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 189,
                        "y": 126
                    },
                    {
                        "x": 189,
                        "y": 139
                    },
                    {
                        "x": 189,
                        "y": 139
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 107,
                        "y": 135
                    },
                    {
                        "x": 109,
                        "y": 135
                    },
                    {
                        "x": 109,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 107,
                        "y": 137
                    },
                    {
                        "x": 109,
                        "y": 137
                    },
                    {
                        "x": 109,
                        "y": 137
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 147,
                        "y": 135
                    },
                    {
                        "x": 149,
                        "y": 135
                    },
                    {
                        "x": 149,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 147,
                        "y": 137
                    },
                    {
                        "x": 149,
                        "y": 137
                    },
                    {
                        "x": 149,
                        "y": 137
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 187,
                        "y": 135
                    },
                    {
                        "x": 189,
                        "y": 135
                    },
                    {
                        "x": 189,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 187,
                        "y": 137
                    },
                    {
                        "x": 189,
                        "y": 137
                    },
                    {
                        "x": 189,
                        "y": 137
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 107,
                        "y": 133
                    },
                    {
                        "x": 114,
                        "y": 133
                    },
                    {
                        "x": 114,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 113,
                        "y": 126
                    },
                    {
                        "x": 113,
                        "y": 133
                    },
                    {
                        "x": 113,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 147,
                        "y": 133
                    },
                    {
                        "x": 154,
                        "y": 133
                    },
                    {
                        "x": 154,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 153,
                        "y": 126
                    },
                    {
                        "x": 153,
                        "y": 133
                    },
                    {
                        "x": 153,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 187,
                        "y": 133
                    },
                    {
                        "x": 194,
                        "y": 133
                    },
                    {
                        "x": 194,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 193,
                        "y": 126
                    },
                    {
                        "x": 193,
                        "y": 133
                    },
                    {
                        "x": 193,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 126,
                        "y": 134
                    },
                    {
                        "x": 127,
                        "y": 134
                    },
                    {
                        "x": 127,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 166,
                        "y": 134
                    },
                    {
                        "x": 166,
                        "y": 134
                    },
                    {
                        "x": 167,
                        "y": 134
                    },
                    {
                        "x": 167,
                        "y": 133
                    },
                    {
                        "x": 167,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 206,
                        "y": 134
                    },
                    {
                        "x": 207,
                        "y": 134
                    },
                    {
                        "x": 207,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 205,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 141
                    },
                    {
                        "x": 40,
                        "y": 149
                    },
                    {
                        "x": 92,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 104,
                        "y": 150
                    },
                    {
                        "x": 104,
                        "y": 149
                    },
                    {
                        "x": 104,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 105,
                        "y": 119
                    },
                    {
                        "x": 105,
                        "y": 120
                    },
                    {
                        "x": 105,
                        "y": 120
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 192,
                        "y": 133
                    },
                    {
                        "x": 192,
                        "y": 135
                    },
                    {
                        "x": 194,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 60,
                        "y": 135
                    },
                    {
                        "x": 60,
                        "y": 146
                    },
                    {
                        "x": 130,
                        "y": 146
                    },
                    {
                        "x": 130,
                        "y": 133
                    },
                    {
                        "x": 133,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 37,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 154,
                        "y": 135
                    },
                    {
                        "x": 153,
                        "y": 135
                    },
                    {
                        "x": 153,
                        "y": 147
                    },
                    {
                        "x": 37,
                        "y": 147
                    },
                    {
                        "x": 37,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 36,
                        "y": 134
                    },
                    {
                        "x": 36,
                        "y": 145
                    },
                    {
                        "x": 113,
                        "y": 145
                    },
                    {
                        "x": 113,
                        "y": 135
                    },
                    {
                        "x": 114,
                        "y": 135
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 61,
                        "y": 113
                    },
                    {
                        "x": 61,
                        "y": 144
                    },
                    {
                        "x": 89,
                        "y": 144
                    },
                    {
                        "x": 89,
                        "y": 133
                    },
                    {
                        "x": 93,
                        "y": 133
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 141,
                        "y": 142
                    },
                    {
                        "x": 141,
                        "y": 149
                    },
                    {
                        "x": 141,
                        "y": 149
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 173,
                        "y": 133
                    },
                    {
                        "x": 170,
                        "y": 133
                    },
                    {
                        "x": 170,
                        "y": 148
                    },
                    {
                        "x": 33,
                        "y": 148
                    },
                    {
                        "x": 33,
                        "y": 131
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 45,
                        "y": 117
                    },
                    {
                        "x": 40,
                        "y": 117
                    },
                    {
                        "x": 40,
                        "y": 117
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 48,
                        "y": 117
                    },
                    {
                        "x": 62,
                        "y": 117
                    },
                    {
                        "x": 62,
                        "y": 117
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 45,
                        "y": 140
                    },
                    {
                        "x": 40,
                        "y": 140
                    },
                    {
                        "x": 40,
                        "y": 140
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 48,
                        "y": 140
                    },
                    {
                        "x": 62,
                        "y": 140
                    },
                    {
                        "x": 62,
                        "y": 123
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 146
                    },
                    {
                        "x": 9,
                        "y": 146
                    },
                    {
                        "x": 10,
                        "y": 146
                    },
                    {
                        "x": 10,
                        "y": 143
                    },
                    {
                        "x": 10,
                        "y": 143
                    },
                    {
                        "x": 21,
                        "y": 143
                    },
                    {
                        "x": 21,
                        "y": 143
                    },
                    {
                        "x": 21,
                        "y": 145
                    },
                    {
                        "x": 22,
                        "y": 145
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 9,
                        "y": 147
                    },
                    {
                        "x": 10,
                        "y": 147
                    },
                    {
                        "x": 10,
                        "y": 150
                    },
                    {
                        "x": 10,
                        "y": 150
                    },
                    {
                        "x": 21,
                        "y": 150
                    },
                    {
                        "x": 21,
                        "y": 148
                    },
                    {
                        "x": 22,
                        "y": 148
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 148
                    },
                    {
                        "x": 14,
                        "y": 150
                    },
                    {
                        "x": 14,
                        "y": 150
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 14,
                        "y": 144
                    },
                    {
                        "x": 14,
                        "y": 143
                    },
                    {
                        "x": 14,
                        "y": 143
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 16,
                        "y": 143
                    },
                    {
                        "x": 16,
                        "y": 142
                    },
                    {
                        "x": 16,
                        "y": 142
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 16,
                        "y": 150
                    },
                    {
                        "x": 16,
                        "y": 151
                    },
                    {
                        "x": 16,
                        "y": 151
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 147,
                        "y": 29
                    },
                    {
                        "x": 155,
                        "y": 29
                    },
                    {
                        "x": 155,
                        "y": 28
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 135,
                        "y": 30
                    },
                    {
                        "x": 127,
                        "y": 30
                    },
                    {
                        "x": 127,
                        "y": 30
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 193,
                        "y": 27
                    },
                    {
                        "x": 193,
                        "y": 25
                    },
                    {
                        "x": 193,
                        "y": 25
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 185,
                        "y": 33
                    },
                    {
                        "x": 193,
                        "y": 33
                    },
                    {
                        "x": 193,
                        "y": 32
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 193,
                        "y": 44
                    },
                    {
                        "x": 193,
                        "y": 42
                    },
                    {
                        "x": 193,
                        "y": 42
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 193,
                        "y": 49
                    },
                    {
                        "x": 193,
                        "y": 54
                    },
                    {
                        "x": 193,
                        "y": 54
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 130,
                        "y": 83
                    },
                    {
                        "x": 131,
                        "y": 83
                    },
                    {
                        "x": 131,
                        "y": 83
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 130,
                        "y": 105
                    },
                    {
                        "x": 131,
                        "y": 105
                    },
                    {
                        "x": 131,
                        "y": 105
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 40,
                        "y": 64
                    },
                    {
                        "x": 40,
                        "y": 72
                    },
                    {
                        "x": 40,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 50,
                        "y": 64
                    },
                    {
                        "x": 50,
                        "y": 72
                    },
                    {
                        "x": 50,
                        "y": 72
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 66
                    },
                    {
                        "x": 50,
                        "y": 66
                    },
                    {
                        "x": 50,
                        "y": 66
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 67
                    },
                    {
                        "x": 40,
                        "y": 67
                    },
                    {
                        "x": 40,
                        "y": 67
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 67,
                        "y": 68
                    },
                    {
                        "x": 65,
                        "y": 68
                    },
                    {
                        "x": 65,
                        "y": 52
                    },
                    {
                        "x": 127,
                        "y": 52
                    },
                    {
                        "x": 127,
                        "y": 28
                    },
                    {
                        "x": 135,
                        "y": 28
                    },
                    {
                        "x": 135,
                        "y": 28
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 91,
                        "y": 18
                    },
                    {
                        "x": 91,
                        "y": 16
                    },
                    {
                        "x": 91,
                        "y": 16
                    }
                ],
                "type": "wire"
            },
            {
                "points": [
                    {
                        "x": 204,
                        "y": 104
                    },
                    {
                        "x": 206,
                        "y": 104
                    },
                    {
                        "x": 206,
                        "y": 107
                    }
                ],
                "type": "wire"
            }
        ]
    }
}
